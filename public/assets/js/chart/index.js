// $(document).ready(function() {
//   $('#highcharts-container').find('[text-anchor="end"]').addClass('active');
// })

$(function() {
    $('#pie-chart').highcharts({
        chart: {
            backgroundColor: 'transparent',
            plotBackgroundColor: 'transparent',
            plotShadow: false,
            type: 'pie',
            color:'white'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
            backgroundColor: 'black',
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true

            }
        },
        series: [{
            name: ' ',
            colorByPoint: true,
            data: [{
                    name: 'Advance sale',
                    y: 20,
                    sliced: true,
                    selected: true,
                    color: '#3bc7d7',
                    asdfdfs:'50000000 tokem'
                }, {
                    name: 'Sale during regular ICO process',
                    y: 40,
                    color: '#ffd200',
                }, {
                    name: 'Bonus pool and bounty program',
                    y: 4,
                    color: '#ED676F',
                }, {
                    name: 'Founders Pot',
                    y: 15,
                    color: '#B0E87C',
                }, {
                    name: 'Reserves LOOiX Management AG ',
                    y: 6,
                    color: '#60AAF6',
                }, {
                    name: 'Sales reserves according to project requirement',
                    y: 15,
                    color: '#FDB45C',
                },
                // {
                //     name: 'Referral Program',
                //     y: 2,
                //     color: '#2DCB75',
                // }
            ]
        }]
    });
});