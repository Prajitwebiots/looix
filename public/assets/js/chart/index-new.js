$(function () {
    var chart;
    var idHolder ='js-container'
    var test = [
                ["Advance sale = 50'000'000'000 Token", 20], 
                ["Sale during regular ICO process = 100'000'000'000 Token ",  40],
                ["Bonus pool and bounty program = 10'000'000'000 Token", 4],
                ["Founders Pot = 37'500'000'000 Token", 15],
                ["Reserves LOOiX Management AG = 15'000'000'000 Token", 6],
                ["Sales reserves according to project requirement = 37'500'000'000 Token (frozen for the time being)", 15]
            ]
    
    $(document).ready(function () {
    	
    	// Build the chart
        this.plot1 = new Highcharts.Chart({  //$('#'+ idHolder).highcharts({
            chart: {
              renderTo: idHolder,
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              backgroundColor: 'transparent',
            plotBackgroundColor: 'transparent',
            },
            credits: { enabled: false },
            colors: ["#3BC7D7", "#FFEB19", "#FF8088", "#C9FF95", "#79C3FF", "#FFCD75", "#FFCD75"],
            tooltip: {
        	    formatter: function () {
                        return '<b>' + this.point.name + '</b>:<br>' + Highcharts.numberFormat(this.y, 0) + ' <b>:</b> ' + Highcharts.numberFormat(this.percentage, 2) + ' %';
                    },
                    backgroundColor: 'black',
            },
            title: {
                    text: null
            },
            plotOptions: {
                pie: {
                  size:'60%',
                  center:['50%','30%'],
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                    enabled: true,
                    //connectorWidth: 0,
                    distance: 10,//-50,
                    color: 'black',
                    formatter: function () {
                      return this.percentage.toFixed(2) + '%';
                    }
                  },
                  showInLegend: true
                }
            },
          legend: {
                    itemStyle: {
                        fontSize: '15px',
                        color: '#0d4480'
                    },
                    enabled: true,
                    layout: 'vertical',
                    align: 'left',
                    verticalAlign: "bottom", //'middle', //'bottom',//'middle'
		                floating:true
                },
          
            series: [{
                type: 'pie',
                name: '',
                innerSize: '50%',
                data: test,
            }]
        });
    });
    
});