var speakeasy = require('speakeasy');
var QRCode = require('qrcode');
var bodyParser = require('body-parser');
var urlencodeParser = bodyParser.urlencoded({ extended: false });
var mongoose=require('mongoose');
var user_model = require('../models/User.js');
var User = mongoose.model('User',user_model.userSchema);

module.exports=function(app){

	app.get('/create-2fa' , function(req,res){
		var secret = speakeasy.generateSecret({length: 20});
		//console.log(req.user._id);
 
		User.findByIdAndUpdate({_id: req.user._id},
	           {
			   	  g_2fa:secret.otpauth_url, 
			   	  g_2fa_base32:secret.base32
			   }, function(err, docs){
			 	if(err) res.json(err);
				else
				{ 
				  QRCode.toDataURL(secret.otpauth_url, function(err, image_data) {
				  	var arr = {};
					arr.image_url=image_data;
					arr.secret=secret.base32;

				  res.send(arr);
				});
				 }
			 });
	})
	app.get('/validate' , function(req,res){
		res.locals = {
            title: 'LOOiX | Validate'
          };
		res.render('auth/validate',{user : req.user,message: req.flash('message'),error: req.flash('error')});
	});

	app.post('/validate' ,urlencodeParser, function(req,res){
		sess=req.session;
		if(req.body.otp_2fa=='')
		{
			req.flash('error','Please enter OTP First.');
			res.redirect('/validate');
		}
		else
		{
				//console.log(req.user);
			var userToken= req.body.otp_2fa;
			var base32secret = req.user.g_2fa_base32;
			var verified = speakeasy.totp.verify({ secret: base32secret,
	                                       encoding: 'base32',
	                                       token: userToken });
			//console.log(verified);
		    if(verified==true){
		    	sess=req.session;
		  		sess.is_2fa_otp_validate = 0;
		  		console.log(req.user);
		    	
		    	if(req.user.role==1){
 					res.redirect('/dashboard');
				 }
				 else if(req.user.role == 0){
				 	res.redirect('/admin-dashboard');
				 }
		    }
		    else
		    {
		    	req.flash('error','Oops! Invalid OTP.');
		    	res.redirect('/validate');
		    }
			
		}

	}) 
	app.post('/validate-otp' ,urlencodeParser, function(req,res){
		
		var userToken= req.body.totp;
		//console.log(userToken);
		var base32secret = req.user.g_2fa_base32;
		var verified = speakeasy.totp.verify({ secret: base32secret,
                                       encoding: 'base32',
                                       token: userToken });
		res.send(verified);
	})
	app.post('/enable-2fa' , function(req,res){
		User.findByIdAndUpdate({_id: req.user._id},
	           {
			   	  isset_2fa:1, 
			   }, function(err, docs){
			 	if(err) { res.json(err); }
				else
				{
					 req.flash('message','Google 2FA Enabled Successfully.');
					 if(req.user.role==1){
 						res.redirect('/profile');
					 }
					 else if(req.user.role == 0){
					 	res.redirect('/admin-profile');
					 }
				}
		});
	});
	app.post('/disable-2fa' , function(req,res){
		User.findByIdAndUpdate({_id: req.user._id},
	           {
			   	  isset_2fa:0, 
			   	  g_2fa:'', 
			   	  g_2fa_base32:''

			   }, function(err, docs){
			 	if(err) { res.json(err); }
				else
				{
					 req.flash('message','Google 2FA Disabled Successfully.');
					if(req.user.role==1){
 						res.redirect('/profile');
					 }
					 else if(req.user.role == 0){
					 	res.redirect('/admin-profile');
					 }
				}
		});
	});
}





