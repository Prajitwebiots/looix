var Coinpayments = require('coinpayments');
var CoinAddress = require('../models/CoinAddress.js');
var Deposit = require('../models/Deposit.js');
var Withdraw = require('../models/Withdraw.js');
var User = require('../models/User.js');
var QRCode = require('qrcode');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var urlencodeParser = bodyParser.urlencoded({ extended: false });
var paypal = require('paypal-rest-sdk');
require('./configure.js');
var options = require('./coinpayment_config.js');
//console.log(options);
var ejs = require("ejs");
var mail = require('../mail.js');
var fs = require('fs');
var client = new Coinpayments(options);


module.exports = function(app) {
    // callback
    let events = Coinpayments.events;
    let middleware = [
        Coinpayments.ipn({
            'merchantId': '44eb18ec9cb7bbd5e7ed83d4737db72d',
            'merchantSecret': 'web1234!@#'
        }),
        function(req, res, next) {}
    ]

    app.use('/deposit_ipn', middleware);

    events.on('ipn_fail', function(data) {

        if(data.ipn_type=="withdrawal"){    // withdraw code start
            //return user amount update status
            Withdraw.findOne({tx_id:data.id},function(err,withdraw){
                if(err){}
                else{ 
                    User.findOne({ _id: withdraw.user_id},function(err,withdrawuser){
                        if(err){}
                         else{
                             var available_bal=0;
                            if( withdraw.currency =='BTC' ){
                                var available_bal=withdrawuser.btc_bal;
                                var available_bonus=withdrawuser.btc_bonus_bal;
                                var coin_bal_name = 'btc_bal';
                                var coin_bonus_bal = 'btc_bonus_bal';
                            }
                            else if(withdraw.currency =='ETH'){
                                 var available_bal=withdrawuser.btc_bal;
                                var available_bonus=withdrawuser.btc_bonus_bal;
                                var coin_bal_name = 'btc_bal';
                                var coin_bonus_bal = 'btc_bonus_bal';
                            }
                            var user_total_bal = parseFloat(available_bal) + parseFloat(withdraw.amount);
                            var user_total_bonus = parseFloat(available_bonus) + parseFloat(withdraw.amount);

                            User.findByIdAndUpdate({_id:withdraw.user_id},
                                {
                                    [coin_bal_name]:user_total_bal,
                                    [coin_bonus_bal]:user_total_bonus
                                },
                                function(err,response){
                                if(err){}
                                else{
                                    Withdraw.findByIdAndUpdate({_id:withdraw._id},{status:3},function(err,result){
                                        if(err){}
                                         else{
                                            console.log("withdraw fail from coin payment");
                                         }   
                                    })
                                }    
                            })
                         }   
                    })                    
                }
            })


        } //withdraw code end
        else {                              //deposit code start

           CoinAddress.findOne({ 'address': data.address }, function(err, address) {
            if (address) {
                Deposit.findOne({ 'tx_id': data.txn_id, 'status': 0 }, function(err, user) {
                    if (user) {
                        if (data.status == -1) {
                            Deposit.findByIdAndUpdate({ _id: user._id }, {
                                status: '2'
                            }, function(err, depositdata) {
                                if (err) {} else {}
                            });
                        }
                    } else {

                        User.findOne({ 'id': user.user_id }, function(err, userbalance) {
                            if(err){

                            }
                            else{
                                var deposit = new Deposit;
                                deposit.user_id = address.user_id;
                                deposit.amount = data.amount;
                                deposit.address = data.address;
                                deposit.tx_id = data.txn_id;
                                deposit.currency = data.currency;
                                deposit.users = userbalance;
                                deposit.save(function(err) {
                                    if (err) {
                                       // console.log(err);
                                    } else {
                                        Deposit.findOne({ 'tx_id': data.txn_id, 'status': 0 }, function(err, result) {
                                            if (result) {
                                                if (data.status == -1) {
                                                    Deposit.findByIdAndUpdate({ _id: result._id }, {
                                                        status: '2'
                                                    }, function(err, status) {
                                                        if (err) {} else {}
                                                    });
                                                }

                                            }
                                        });
                                    }
                                }); 
                            }
                        })
                    }

                });

            }

        });
    } //deposit code end

    });

    events.on('ipn_pending', function(data) {

    });

    events.on('ipn_complete', function(data) {

        if (data.currency == 'BTC') {
            var balance = data.amount;
            var coin_bal_name = "btc_bal";
        } else if (data.currency == 'ETH') {
            var balance = data.amount;
            var coin_bal_name = "eth_bal";
        }

        if(data.ipn_type=="withdrawal"){    // withdraw code start
            // update status

            Withdraw.findOne({tx_id:data.id},function(err,withdraw){
                if(err){}
                else{ 
                    Withdraw.findByIdAndUpdate({_id:withdraw._id},{status:1},function(err,result){
                        if(err){}
                         else{
                            console.log("withdraw success");
                         }   
                    })
                }
            })
        } //withdraw code end
        else {  //deposit code start
        CoinAddress.findOne({ 'address': data.address }, function(err, address) {
            if (address) {
                User.findOne({ '_id': address.user_id }, function(err, userbalance) {
                    if(userbalance){
                Deposit.findOne({ 'tx_id': data.txn_id, 'status': 0 }, function(err, user) {
                     if (user) {
                  
                        if (data.status >= 100) {
                           
                                if (data.currency == 'BTC') {
                                    var mybal = userbalance.btc_bal;
                                } else if (data.currency == 'ETH') {
                                    var mybal = userbalance.eth_bal;
                                }

                                Deposit.findByIdAndUpdate({ _id: user._id }, {
                                    status: '1'
                                }, function(err, depositdata) {
                                    if (err) {} else {
                                        var total_amount = parseFloat(mybal) + parseFloat(balance);
                                        User.findByIdAndUpdate({ _id: user.user_id }, {
                                            [coin_bal_name]: total_amount
                                        }, function(err, userdata) {
                                            if (err) {} else {}
                                        });
                                    }
                                });
                        }
                   
                    } else {
                        var deposit = new Deposit;
                        deposit.user_id = address.user_id;
                        deposit.amount = data.amount;
                        deposit.address = data.address;
                        deposit.tx_id = data.txn_id;
                        deposit.currency = data.currency;
                        deposit.users = userbalance;
                        deposit.save(function(err) {
                            if (err) {
                               // console.log(err);
                            } else {
                                Deposit.findOne({ 'tx_id': data.txn_id, 'status': 0 }, function(err, result) {
                                    if (result) {
                                        if (data.status >= 100) {
                                            User.findOne({ '_id': address.user_id }, function(err, userbal) {
                                                if (data.currency == 'BTC') {
                                                    var mybalc = userbal.btc_bal;
                                                } else if (data.currency == 'ETH') {
                                                    var mybalc = userbal.eth_bal;
                                                }

                                                Deposit.findByIdAndUpdate({ _id: result._id }, {
                                                    status: '1'
                                                }, function(err, depositdata) {
                                                    if (err) {} else {
                                                        var total_amount = parseFloat(mybalc) + parseFloat(balance);
                                                        User.findByIdAndUpdate({ _id: address.user_id }, {
                                                            [coin_bal_name]: total_amount
                                                        }, function(err, userdata) {
                                                            if (err) {} else {}
                                                        });
                                                    }
                                                });
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    
                          // user
                    }//
                });
 
                 }
                    else{
                        console.log("No user present");
                    }
                });
            }

        });
       }//deposit code end
    });


    // end


    app.get('/wallet', global.isUserAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Wallet'
        };
        res.render('user/wallet/index', { user: req.user });
    })

    app.get('/deposit/:coin', global.isUserAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Deposit'
        };
        var user_id = req.user._id;
        var coin = req.params.coin;

        Deposit.find({ user_id: user_id, currency: req.params.coin }).sort({ _id: -1 }).exec(function(err, deposit) {
            if (err) {} else {
                // console.log(deposit);
                CoinAddress.findOne({ 'user_id': user_id, 'coin': coin }, function(err, data) {
                    if (err) throw err;
                    if (!data || data.length == 0) {
                        client.getCallbackAddress(coin, function(err, response) {
                            if (response.address) {
                                CoinAddress({ user_id: user_id, coin: coin, address: response.address, users : req.user }).save(function(err, coinaddress) {
                                    if (err) throw err;
                                    else
                                        QRCode.toDataURL(coinaddress.address, function(err, image_data) {
                                            res.render('user/wallet/deposit', { data: image_data, deposit: deposit, address: coinaddress.address, user: req.user, coin: coin });
                                        });
                                })
                            }
                        })
                    } else {
                        QRCode.toDataURL(data.address, function(err, image_data) {
                            res.render('user/wallet/deposit', { data: image_data, deposit: deposit, address: data.address, user: req.user, coin: coin });
                        });
                    }
                })


            }
        })
    })


    // app.get('/deposit-fiat/:coin', global.isUserAllowed, function(req, res) {
    //     res.locals = {
    //         title: 'Looix | Deposit'
    //     };
    //     res.render('user/wallet/deposit-fiat', { user: req.user, coin: req.params.coin, 'success': req.flash('success'), 'error': req.flash('error') });
    // })

    // app.post('/deposit-fiat/:coin', global.isUserAllowed, urlencodeParser, function(req, res) {

    //     var deposit = new Deposit;
    //     deposit.user_id = req.user._id;
    //     deposit.amount = req.body.fiat_amount;
    //     deposit.tx_id = randomString(32);
    //     deposit.currency = req.params.coin;
    //     // save the user
    //     deposit.save(function(err) {
    //         if (err) {
    //             console.log("err" + err);
    //             req.flash('error', 'Error in Saving Deposit' + err)
    //             res.sendStatus(false);
    //         } else {
    //             req.flash('success', 'Your request for deposit send to admin successfully.')
    //             res.redirect('/deposit-fiat/' + req.params.coin);
    //         }
    //     })
    // })

    app.get('/deposit-fiat/:coin', global.isUserAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Deposit'
        };
        Deposit.find({ user_id: req.user._id, currency: req.params.coin }).sort({ _id: -1 }).exec(function(err, deposit) {
            if (err) {} else {
                res.render('user/wallet/deposit-fiat-paypal', { user: req.user, deposit: deposit, coin: req.params.coin, 'success': req.flash('success'), 'error': req.flash('error') });
            }
        })
    })

    // app.post('/deposit-fiat/:coin', global.isUserAllowed, urlencodeParser, function(req, res) {

    // var create_payment_json = {
    //     "intent": "sale",
    //     "payer": {
    //         "payment_method": "paypal"
    //     },
    //     "redirect_urls": {
    //         "return_url": "http://return.url",
    //         "cancel_url": "http://cancel.url"
    //     },
    //     "transactions": [{
    //         "item_list": {
    //             "items": [{
    //                 "name": "item",
    //                 "sku": "item",
    //                 "price": req.body.fiat_amount,
    //                 "currency": req.params.coin,
    //                 "quantity": 1
    //             }]
    //         },
    //         "amount": {
    //             "currency": req.params.coin,
    //             "total": req.body.fiat_amount
    //         },
    //         "description": "This is the payment description."
    //     }]
    // };


    // paypal.payment.create(create_payment_json, function (error, payment) {
    //     if (error) {
    //         throw error;
    //     } else {
    //         console.log("Create Payment Response");
    //         console.log(payment);
    //     }
    // });
    // var deposit = new Deposit;
    // deposit.user_id = req.user._id;
    // deposit.amount = req.body.fiat_amount;
    // deposit.tx_id = "kjaswhdjasjhs";
    // deposit.currency = req.params.coin;
    // // save the user
    // deposit.save(function(err) {
    //     if (err) {
    //         console.log("err" + err);
    //         req.flash('error', 'Error in Saving Deposit' + err)
    //         res.sendStatus(false);
    //     } else {
    //         req.flash('success', 'Your request for deposit send to admin successfully.')
    //         res.redirect('/deposit-fiat/' + req.params.coin);
    //     }

    // })
    //  })
    app.post('/success-order', global.isUserAllowed, urlencodeParser, function(req, res) {
        res.locals = {
            title: 'LOOiX | Deposit'
        };
        var paymentID = req.body.paymentID;
        var paymentToken = req.body.paymentToken;
        var orderID = req.body.orderID;
        var payerID = req.body.payerID;
        var amount = req.body.amount;
        var currency = req.body.currency;
        //inserting data to deposit table
        if (req.body.currency == 'EUR') {
            var balance = req.user.euro_bal;
            var coin_bal_name = "euro_bal";
        } else if (req.body.currency == 'USD') {
            var balance = req.user.usd_bal;
            var coin_bal_name = "usd_bal";
        }
        var deposit = new Deposit;
        deposit.user_id = req.user._id;
        deposit.amount = amount;
        deposit.payment_token = paymentToken;
        deposit.payment_id = paymentID;
        deposit.order_id = orderID;
        deposit.payer_id = payerID;
        deposit.currency = currency;
        deposit.status = 1;
        deposit.users = req.user;
        // save the user
        deposit.save(function(err) {
            if (err) {
                //console.log("err"+err);
                req.flash('error', 'Error in Saving Deposit' + err)
                res.send("error");
            } else {
                var total_amount = parseFloat(balance) + parseFloat(amount);
                // console.log(amount+" "+total_amount);
                User.findByIdAndUpdate({ _id: req.user._id }, {
                    [coin_bal_name]: total_amount
                }, function(err, data) {
                    if (err) {} else {
                        // console.log(data);
                        res.send("done");
                    }
                });
            }

        });


    })

    app.get('/withdraw/:coin', global.isUserAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Withdraw'
        };
        Withdraw.find({ user_id: req.user._id, currency: req.params.coin }).sort({ _id: -1 }).exec(function(err, data) {
            if (err) {} else {
                //console.log(data);
                res.render('user/wallet/withdraw', { user: req.user, coin: req.params.coin, withdraw: data, 'success': req.flash('success'), 'error': req.flash('error') });
            }
        })


    })

    app.post('/withdraw/:coin', global.isUserAllowed, function(req, res) {
        if (req.body.coin_address == '') {
            req.flash('error', 'Address fields is require');
            res.redirect('/withdraw/' + req.params.coin);
        } else if (req.body.amount == '') {
            req.flash('error', 'Amount fields is require');
            res.redirect('/withdraw/' + req.params.coin);
        } else {
            if (req.params.coin == "BTC") { var available_bonus = req.user.btc_bonus_bal; } else if (req.params.coin == "ETH") { var available_bonus = req.user.btc_bonus_bal; }
            if (available_bonus >= req.body.amount) {
                var withdraw = new Withdraw;
                withdraw.user_id = req.user._id;
                withdraw.amount = req.body.amount;
                withdraw.tx_id = randomString(32);
                withdraw.currency = req.params.coin;
                withdraw.address = req.body.coin_address;
                withdraw.users = req.user;
                // save the user
                withdraw.save(function(err) {
                    if (err) {
                        //console.log("err" + err);
                        req.flash('error', 'Error in Saving withdraw' + err)
                        res.redirect('/withdraw/' + req.params.coin);
                    } else {
                        req.flash('success', 'Your request for withdraw send to admin successfully.')
                        res.redirect('/withdraw/' + req.params.coin);
                    }
                })
            } else {
                req.flash('error', 'Insufficient Bonus Balance');
                res.redirect('/withdraw/' + req.params.coin);
            }
        }
    })

    app.get('/withdraw-fiat/:coin', global.isUserAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Withdraw'
        };

        Withdraw.find({ user_id: req.user._id, currency: req.params.coin }).sort({ _id: -1 }).exec(function(err, data) {
            if (err) {} else {
                res.render('user/wallet/withdraw-fiat', { user: req.user, coin: req.params.coin, withdraw: data, 'success': req.flash('success'), 'error': req.flash('error') });
            }
        })

    })

    app.post('/withdraw-fiat/:coin', global.isUserAllowed, function(req, res) {

        if (req.user.paypal_email == "") {
            req.flash('error', 'Please first set your Paypal email id in your profile.');
            res.redirect('/withdraw-fiat/' + req.params.coin);
        } else {
            if (req.params.coin == 'EUR') {
                var bonus_balance = req.user.eur_bonus_bal;
                var balance = req.user.euro_bal;
                var bonus_coin_name = "eur_bonus_bal";
                var coin_bal_name = "euro_bal";
            } else if (req.params.coin == 'USD') {
                var bonus_balance = req.user.usd_bonus_bal;
                var balance = req.user.usd_bal;
                var bonus_coin_name = "usd_bonus_bal";
                var coin_bal_name = "usd_bal";
            }
            if (req.body.fiat_amount > bonus_balance) {
                req.flash('error', 'Insufficient Bonus balance for ' + req.params.coin);
                res.redirect('/withdraw-fiat/' + req.params.coin);
            } else {
                var sender_batch_id = Math.random().toString(36).substring(9);
                var create_payout_json = {
                    "sender_batch_header": {
                        "sender_batch_id": sender_batch_id,
                        "email_subject": "You have a payment"
                    },
                    "items": [{
                        "recipient_type": "EMAIL",
                        "amount": {
                            "value": req.body.fiat_amount,
                            "currency": req.params.coin
                        },
                        "receiver": req.user.paypal_email,
                        "note": "Thank you.",
                        "sender_item_id": "item_1"
                    }]
                };
                paypal.payout.create(create_payout_json, function(error, payout) {
                    if (error) {
                        //console.log(error);
                        if (error.httpStatusCode >= 400 || error.httpStatusCode <= 499) {
                            req.flash('error', error.response.message);
                            res.redirect('/withdraw-fiat/' + req.params.coin);
                        } else if (error.httpStatusCode >= 500 || error.httpStatusCode <= 599) {
                            req.flash('error', 'Server error! Please try again.');
                            res.redirect('/withdraw-fiat/' + req.params.coin);
                        } else {
                            req.flash('error', "Request incomplete! Please try again.");
                            res.redirect('/withdraw-fiat/' + req.params.coin);
                        }

                        //throw error;
                    } else {

                        var left_bonus_amount = bonus_balance - req.body.fiat_amount;
                        var left_amount = balance - req.body.fiat_amount;

                        User.findByIdAndUpdate({ _id: req.user._id }, {
                            [bonus_coin_name]: left_bonus_amount,
                            [coin_bal_name]: left_amount
                        }, function(err, data) {
                            if (err) {} else {
                                var withdraw = new Withdraw;
                                withdraw.user_id = req.user._id;
                                withdraw.amount = req.body.fiat_amount;
                                withdraw.currency = req.params.coin;
                                withdraw.payout_batch_id = payout.batch_header.payout_batch_id;
                                withdraw.address = "";
                                withdraw.status = 1;
                                withdraw.users = req.user;
                                withdraw.save(function(err) {
                                    if (err) {
                                        // console.log("err" + err);
                                        req.flash('error', 'Error in Saving withdraw' + err)
                                        res.redirect('/withdraw/' + req.params.coin);
                                    } else {
                                        req.flash('success', 'Withdraw done successfully');
                                        res.redirect('/withdraw-fiat/' + req.params.coin);
                                    }
                                })
                            }
                        })
                    }
                });
            }
        }
    })

    app.get('/withdraw-looix', global.isUserAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Withdraw'
        };
        Withdraw.find({ user_id: req.user._id, currency: 'looix' }).sort({ _id: -1 }).exec(function(err, data) {
            if (err) {} else {
                //console.log(data);
                res.render('user/wallet/withdraw-looix', { user: req.user, 'success': req.flash('success'), withdraw: data, 'error': req.flash('error') });
            }
        })

    });

    app.post('/withdraw-looix', global.isUserAllowed, function(req, res) {

        var withdraw = new Withdraw;
        withdraw.user_id = req.user._id;
        withdraw.amount = req.body.amount;
        withdraw.currency = 'looix';
        withdraw.address = req.user.erc20_address;
        withdraw.users = req.user;
        withdraw.save(function(err) {
            if (err) {
                // console.log("err" + err);
                req.flash('error', 'Error in Saving withdraw' + err)
                res.redirect('/withdraw/' + req.params.coin);
            } else {
                var left_token = req.user.total_token - req.body.amount;
                User.findByIdAndUpdate({ _id: req.user._id }, { total_token: left_token }, function(err, data) {
                    if (err) {
                        // console.log("err" + err);
                        req.flash('error', 'Error in Saving withdraw' + err)
                        res.redirect('/withdraw/' + req.params.coin);
                    } else {

                        req.flash('success', 'Your request for withdraw looix token send to admin successfully.')
                        res.redirect('/withdraw-looix');
                    }

                })
            }
        })
    })

    //*******************************************Admin functions********************************************

    //LOOix Withdraw Start
    app.get('/get-withdraw-looix', global.isAdminAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Withdraw History'
        };
        Withdraw.find({ currency: 'looix' }).populate('users').sort({ _id: -1 }).exec( function(err, data) {
            if (err) {

            } else {
                //console.log(data);
                res.render('admin/withdraw/withdraw-looix', { user: req.user, withdraw: data, 'success': req.flash('success'), 'error': req.flash('error') });
            }
        })

    })

    app.get('/withdraw-looix-approve/:id', function(req, res) {
        Withdraw.findByIdAndUpdate({ _id: req.params.id }, {
            status: 1
        }, function(err, docs) {
            if (err) res.send(err);
            else {
                req.flash('success', 'Withdraw request updated Successfully.');
                res.redirect('/get-withdraw-looix');
            }
        });
    });

    app.get('/withdraw-looix-reject/:id', function(req, res) {
        Withdraw.findOne({ _id: req.params.id }, function(err, withdraw) {
            if (err) {} else {
                User.findOne({ _id: withdraw.user_id }, function(err, user) {
                    if (err) {} else {
                        var total_token = user.total_token + withdraw.amount;
                      //  console.log(total_token);
                        User.findByIdAndUpdate({ _id: withdraw.user_id }, { total_token: total_token }, function(err, updtdata) {
                            if (err) {} else {
                                Withdraw.findByIdAndUpdate({ _id: req.params.id }, {
                                    status: 2
                                }, function(err, docs) {
                                    if (err) res.send(err);
                                    else {
                                        req.flash('success', 'Withdraw request updated Successfully.');
                                        res.redirect('/get-withdraw-looix');
                                    }
                                });
                            }
                        })
                    }
                })
            }
        });
    })

    //LOOix Withdraw End

    //Coin Withdraw Start
    app.get('/get-withdraw-coin', global.isAdminAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Withdraw History'
        };
        Withdraw.find({ currency: { $in: [ 'BTC', 'ETH' ] }  }).populate('users').sort({ _id: -1 }).exec( function(err, data) {
            if (err) {

            } else {
                res.render('admin/withdraw/withdraw-coin', { user: req.user, withdraw: data, 'success': req.flash('success'), 'error': req.flash('error') });
            }
        })
    })

    app.get('/withdraw-coin-approve/:id', function(req, res) {
        Withdraw.findOne({ _id: req.params.id }, function(err, withdraw) {
            if (err) {
                req.flash('error', err);
                res.redirect('/get-withdraw-coin');
            } else {
                var coin_amount = withdraw.amount;
                var coin_type = withdraw.currency;
                var user_id = withdraw.user_id;

               // console.log(withdraw.currency);
                client.createWithdrawal({ 'currency': withdraw.currency, 'amount': withdraw.amount, 'address': withdraw.address }, function(err, result) {
                    if (err) 
                    {
                        req.flash('error', err);
                        res.redirect('/get-withdraw-coin');
                       } 
                       else {
                        Withdraw.findByIdAndUpdate({ _id: withdraw._id }, { tx_id: result.id, status: 1 }, function(err, withdraw_success) 
                        {

                            if (err) 
                            {
                                req.flash('error', 'Error during updating withdraw status');
                                res.redirect('/get-withdraw-coin');
                            } else 
                            {

                            User.find({ _id: withdraw.user_id }, function(err, udata) { if (err) { throw err; } 
                                else { 
                                   var user_email = udata.email;
                                      ejs.renderFile("./views/emails/withdraw_status.ejs", { status: "Approve",coin_amount:coin_amount,coin_type:coin_type }, function(err, data) {
                                            var mailOptions = {
                                                from: 'testproject342@gmail.com',
                                                to: user_email,
                                                subject: 'Withdraw Request',
                                                html: data
                                            };
                                            global.transporter.sendMail(mailOptions, function(error, info) {
                                                if (error) { //console.log(error); } else {
                                                       req.flash('success', 'Withdraw request updated Successfully.');
                                                       res.redirect('/get-withdraw-coin');
                                                }
                                            })
                                        });
                                     }  
                            });
                        }
                });
            }
        });
      }
    });
 });

    app.get('/withdraw-coin-reject/:id', function(req, res) {
        Withdraw.findOne({ _id: req.params.id }, function(err, withdraw) {
            var coin_amount = withdraw.amount;
             var coin_type = withdraw.currency;
            if (err) {} else {
                User.findOne({ _id: withdraw.user_id }, function(err, user) {
                    if (err) {} else {
                        if (withdraw.currency == "BTC") {
                            var updt_bonus_bal = user.btc_bonus_bal + withdraw.amount;
                            var updt_bal = user.btc_bal + withdraw.amount;
                            var coin_bonus_name = 'btc_bonus_bal';
                            var coin_name = 'btc_bal';
                        } else if (withdraw.currency == "ETH") {
                            var updt_bonus_bal = user.btc_bonus_bal + withdraw.amount;
                            var updt_bal = user.btc_bal + withdraw.amount;
                            var coin_bonus_name = 'btc_bonus_bal';
                            var coin_name = 'btc_bal';
                        }


                        User.findByIdAndUpdate({ _id: withdraw.user_id }, {
                            [coin_bonus_name]: updt_bonus_bal,
                            [coin_name]: updt_bal
                        }, function(err, updtdata) {

                            var user_email = updtdata.email;
                            if (err) {} else 
                            {
                                Withdraw.findByIdAndUpdate({ _id: req.params.id }, { status: 2 }, function(err, docs) 
                                {
                                    if (err) res.send(err);
                                    else
                                    {
                                          ejs.renderFile("./views/emails/withdraw_status.ejs", { status: "Reject",coin_amount:coin_amount,coin_type:coin_type }, function(err, data) {
                                            var mailOptions = {
                                                from: 'testproject342@gmail.com',
                                                to: user_email,
                                                subject: 'Withdraw Request',
                                                html: data
                                            };
                                            global.transporter.sendMail(mailOptions, function(error, info) {
                                                if (error) { console.log(error); } else {
                                                      req.flash('success', 'Withdraw request updated Successfully.');
                                                      res.redirect('/get-withdraw-coin');
                                                }
                                            })
                                        });
                                      
                                    }
                                });
                            }
                        })
                    }
                })
            }
        });
    })

    //Coin Withdraw End

    //Fiat Withdraw Start
        app.get('/get-withdraw-fiat', global.isAdminAllowed, function(req, res) {
            res.locals = {
                title: 'LOOiX | Withdraw History'
            };
            Withdraw.find({ currency: { $in: [ 'USD', 'EUR' ] } }).populate('users').sort({ _id: -1 }).exec( function(err, data) {
                if (err) {

                } else {
                    res.render('admin/withdraw/withdraw-fiat', { user: req.user, withdraw: data, 'success': req.flash('success'), 'error': req.flash('error') });
                }
            })
        })

    //Fiat Withdraw End

 //Coin Deposit Start
    app.get('/get-deposit-coin', global.isAdminAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Deposit History'
        };
        Deposit.find({ currency: { $in: [ 'BTC', 'ETH' ] } }).populate('users').sort({ _id: -1 }).exec( function(err, data) {
            if (err) {

            } else {
                res.render('admin/deposit/deposit-coin', { user: req.user, deposit: data, 'success': req.flash('success'), 'error': req.flash('error') });
            }
        })
    }) 
    app.get('/get-deposit-fiat', global.isAdminAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Deposit History'
        };
        Deposit.find({ currency:{ $in: [ 'USD', 'EUR' ] }  }).sort({ _id: -1 }).populate('users')
            .exec( function(err, data) {
            if (err) {

            } else {
                res.render('admin/deposit/deposit-fiat', { user: req.user, deposit: data, 'success': req.flash('success'), 'error': req.flash('error') });
            }
        })
    })

 //Coin Deposit end

    function randomString(length) {
        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = length;
        var randomstring = '';
        for (var i = 0; i < length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1);
        }
        return randomstring;
    }

}