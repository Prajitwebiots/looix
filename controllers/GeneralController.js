
const request = require('request');
var Setting = require('../models/Setting.js');
module.exports = function(app) {
app.get('/terms-condition', function(req, res) {
	 res.locals = {
            title: 'LOOiX | Terms'
      };
	res.render('general/terms_condition');	
})

app.get('/privacy-policy', function(req, res) {
	 res.locals = {
        title: 'LOOiX | Policy'
     };
	res.render('general/privacy_policy');		
})

app.get('/coinprice-cron',function(req,res){
	  request('https://www.bitstamp.net/api/v2/ticker/btceur', (error, response, body) => {
        // parse the json answer and get the current bitcoin value
        const data = JSON.parse(body);
         var btc_price= (data.last);
         var myquery = { slug: "setting" };
 		 var newvalues =   {btc_price: btc_price} ;
       	 Setting.updateOne(myquery, newvalues, function(err, res) {
		  });
    	}); 
	   request('https://www.bitstamp.net/api/v2/ticker/etheur', (error, response, body) => {
        // parse the json answer and get the current bitcoin value
        const data = JSON.parse(body);
        var eth_price= (data.last);
         var myquery = { slug: "setting" };
 		 var newvalues =   {eth_price: eth_price} ;
       	 Setting.updateOne(myquery, newvalues, function(err, res) {
		  });
    	}); 
	   request('https://www.bitstamp.net/api/v2/ticker/eurusd', (error, response, body) => {
        // parse the json answer and get the current bitcoin value
        const data = JSON.parse(body);
        var usd_price= (1/data.last);
         var myquery = { slug: "setting" };
 		 var newvalues =   {usd_price: usd_price} ;
       	 Setting.updateOne(myquery, newvalues, function(err, res) {
		  });
    	});
})

};

