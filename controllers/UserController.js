var express = require('express');
var bodyParser = require('body-parser');
var urlencodeParser = bodyParser.urlencoded({ extended: false });
var mongoose = require('mongoose');
var bCrypt = require('bcryptjs');
var validator = require('express-validator');
var multer = require('multer');
var path = require('path');
var passport = require("passport");
const LocalStrategy = require('passport-local').Strategy;
var dbConfig = require('../db.js');
var Setting = require('../models/Setting.js');
var Login = require('../models/Login.js');
mongoose.connect(dbConfig.url);

var User = require('../models/User.js');
var Deposit = require('../models/Deposit.js');
var Withdraw = require('../models/Withdraw.js');

module.exports = function(app) {

    app.get('/dashboard', global.isUserAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Dashboard'
        };


        var url = req.protocol + '://' + req.get('host');



        startDate = new Date();
        startDate.setHours(05, 30, 00, 400);

        endDate = new Date();
        endDate.setHours(29, 29, 00, 400);

        //  console.log('Start : '+startDate+'--End-- : '+endDate);


        Setting.findOne({}, function(err, data) {
            if (err) { throw err; } else {
                Login.find({ created_date: { "$gte": startDate, "$lt": endDate }, user_id: req.user.id }).sort({ _id: -1 }).exec(function(err, logindata) {
                    Withdraw.find({ created_date: { "$gte": startDate, "$lt": endDate }, user_id: req.user.id }).sort({ _id: -1 }).exec(function(err, datawithdraw) {
                        Deposit.find({ created_date: { "$gte": startDate, "$lt": endDate }, user_id: req.user.id }).sort({ _id: -1 }).exec(function(err, depositdata) {
                            if (req.user.role == 1) { res.render('dashboard/dashboard', { user: req.user, url: url, setting: data, depdata: depositdata, withdata: datawithdraw, logindata: logindata }); } else if (req.user.role == 0) { res.render('admin/dashboard', { user: req.user, url: url, setting: data }); } else { res.send("no match"); }
                        })
                    })
                })
            }
        })
    });

    app.get('/profile', global.isUserAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Profile'
        };
        res.render('user/profile', { user: req.user, success: req.flash('success'), error: req.flash('error') });
    });

    app.post('/update-profile', urlencodeParser, function(req, res) {
        var isaddress = global.isAddress(req.body.erc_20_address);
        if (isaddress == 0) {
            req.flash('error', 'Invalid ERC20 Address');
            res.redirect('/profile');
        } else {
            User.findByIdAndUpdate({ _id: req.body.user_id }, {
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                erc20_address: req.body.erc_20_address,
                paypal_email: req.body.paypal_email
            }, function(err, docs) {
                if (err) res.send(err);
                else {
                    // console.log(docs);
                    req.flash('success', 'Profile updated successfully.');
                    res.redirect('/profile');
                }
            });
        }
    })


    app.post('/changepass', urlencodeParser, global.isUserAllowed, function(req, res, next) {
        var password = req.body.old_password;
        if (!isValidPassword(req.user, password)) {
            req.flash('error', 'Invalid Old Password');
            res.redirect('/profile');
        } else {
            if (req.body.new_password !== req.body.confirm_password) {
                //throw new Error('password and confirm password do not match');
                req.flash('error', 'password and confirm password do not match.');
                res.redirect('/profile');
            } else {
                var user = req.user;
                user.password = createHash(req.body.new_password);
                user.save(function(err) {
                    if (err) { next(err) } else {
                        req.flash('success', 'Password updated successfully.');
                        res.redirect('/profile');
                    }
                })
            }
        }
    });


    app.post('/edit-profile', urlencodeParser, global.isUserAllowed, function(req, res, next) {

        var upload = multer({
            storage: storage,
            limits: {
                fileSize: 1000000
            },
            fileFilter: function(req, file, callback) {
                var ext = path.extname(file.originalname)
                if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
                    return callback(req.flash('error', 'Only images are allowed'), null)
                }
                callback(null, true)
            }
        }).single('photo');
        upload(req, res, function(err) {
            if (err) {
                req.flash('error', "!" + err);
                res.redirect('/profile');
            } else {
                User.findByIdAndUpdate({ _id: req.user._id }, {
                    profile_pic: req.file.filename
                }, function(err, docs) {
                    if (err) res.send(err);
                    else {
                        //console.log(docs);
                        req.flash('success', 'Profile is uploaded.');
                        res.redirect('/profile');
                    }
                });
            }
        })
    })

    app.post('/upload-kyc', urlencodeParser, global.isUserAllowed, function(req, res, next) {
        var upload = multer({
            storage: storage_kyc,
            limits: {
                fileSize: 5000000
            }
        }).any();
        upload(req, res, function(err) {
            if (err) {
                req.flash('error', "Opps!" + err);
                res.redirect('/profile');
            } else {
                var kyc_files = [];
                for (var i = 0; i < req.files.length; i++) {
                    if (req.files[i]['fieldname'] == 'first_id_proof') { kyc_files['front'] = req.files[i]['filename']; } else if (req.files[i]['fieldname'] == 'second_id_proof') { kyc_files['backend'] = req.files[i]['filename']; } else if (req.files[i]['fieldname'] == 'third_id_proof') { kyc_files['selfie'] = req.files[i]['filename']; }
                }

                User.findByIdAndUpdate({ _id: req.user._id }, {
                    kyc_front: kyc_files['front'],
                    kyc_backend: kyc_files['backend'],
                    kyc_selfie: kyc_files['selfie']
                }, function(err, docs) {
                    if (err) res.send(err);
                    else {
                        //console.log(docs);
                        req.flash('success', 'KYC is uploaded Successfully.');
                        res.redirect('/profile');
                    }
                });
            }
        })
    })

    // router.post('/',upload.any(),function(req,res,next){
    //     console.log(req.files);
    //     res.send(req.files);
    // });

}; //module end
var isValidPassword = function(user, password) {
    return bCrypt.compareSync(password, user.password);
}
var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}
var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/assets/images/user')
    },
    filename: function(req, file, callback) {
        callback(null, req.user.username + '-' + Date.now() + path.extname(file.originalname))
    }
})

var storage_kyc = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/assets/images/kyc')
    },
    filename: function(req, file, callback) {
        callback(null, req.user._id + '-' + Date.now() + path.extname(file.originalname))
    }
})