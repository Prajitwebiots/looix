
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var urlencodeParser = bodyParser.urlencoded({ extended: false });
var BuyToken = require('../models/BuyToken.js');
var Setting = require('../models/Setting.js');
var User = require('../models/User.js');
var RefRate = require('../models/RefRate.js');
var Referral = require('../models/Referral.js');
require('./configure.js');
var ejs = require("ejs");
var mail = require('../mail.js');

module.exports = function(app) {
    app.get('/buy-token', global.isUserAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | ICO'
        };

        Setting.findOne({}, function(err, data) {
            if (err) { throw err; } else {
                BuyToken.find({ user_id: req.user._id }).sort({ _id: -1 }).exec(function(err, tokens) {
                    if (err) { throw err; } else {
                        res.render('user/ico/buy-token', { tokens: tokens, user: req.user, setting: data, 'success': req.flash('success'), 'error': req.flash('error') });
                    }
                })
            }
        })
    })

    app.post('/storeIco', global.isUserAllowed, urlencodeParser, function(req, res) {

        var tokens = req.body.total_no_of_token; // token requested
        var path = req.protocol + '://' + req.get('host');
       // console.log(path);
        Setting.findOne({}, function(err, setting) {

            if (err) { throw err; } else {

                var rate = setting.rate;
                if (req.body.coin == 'BTC') {
                    var coin_price = setting.btc_price;
                    var mycoinbal = req.user.btc_bal;
                    var coin_bal_name = 'btc_bal';
                    var coin_bonus_name = 'btc_bonus_bal';
                    var coin_bonus_bal =req.user.btc_bonus_bal;
                    var coin_ref_bonus_name = 'ref_btc_bonus_bal';
                    var token = tokens * rate;
                    var total_coin = (token / coin_price).toFixed(8);

                } else if (req.body.coin == 'ETH') {
                    var coin_price = setting.eth_price;
                    var mycoinbal = req.user.eth_bal;
                    var coin_bal_name = 'eth_bal';
                    var coin_bonus_name = 'eth_bonus_bal';
                    var coin_bonus_bal =req.user.eth_bonus_bal;
                    var coin_ref_bonus_name = 'ref_eth_bonus_bal';
                    var token = tokens * rate;
                    var total_coin = (token / coin_price).toFixed(8);

                } else if (req.body.coin == 'USD') {
                    var coin_price = setting.usd_price;
                    var mycoinbal = req.user.usd_bal;
                    var coin_bal_name = 'usd_bal';
                    var coin_bonus_name = 'usd_bonus_bal';
                    var coin_bonus_bal =req.user.usd_bonus_bal;
                    var coin_ref_bonus_name = 'ref_usd_bonus_bal';
                    var token = tokens * rate;
                    var total_coin = (token / coin_price).toFixed(4);

                } else if (req.body.coin == 'EUR') {
                    var coin_price = setting.euro_price;
                    var mycoinbal = req.user.euro_bal;
                    var coin_bal_name = 'euro_bal';
                    var coin_bonus_name = 'eur_bonus_bal';
                    var coin_bonus_bal =req.user.eur_bonus_bal;
                    var coin_ref_bonus_name = 'ref_eur_bonus_bal';
                    var token = tokens * rate;
                    var total_coin = token.toFixed(2);
                }

                if (mycoinbal >= total_coin || req.body.total_no_of_token <= '2000') {

                    var tx_id = randomString();
                    var buy_token = new BuyToken();
                    buy_token.user_id = req.user._id;
                    buy_token.ref_user_id = req.user.parent;
                    buy_token.tx_id = 'L' + tx_id;
                    buy_token.amount = total_coin;
                    buy_token.tokens = req.body.total_no_of_token;
                    buy_token.type = req.body.coin; 
                    buy_token.users = req.user;

                    // save the user
                    var buytoken_id=0;
                    buy_token.save(function(err,data) {
                        if (err) {
                            req.flash('error', 'Please Try again' + err)
                            res.redirect('buy-token');
                        }
                        else{
                          //console.log(data);
                        buytoken_id=data._id;  
                        }
                        
                    });
                  
                    // Update Looix Token balance in user table
                    var coin_bal = mycoinbal - total_coin;
                    if(coin_bal < coin_bonus_bal){
                        var new_coin_bonus_bal = coin_bal;
                    }
                    else{
                        var new_coin_bonus_bal = coin_bonus_bal;
                    }
                    var total_token = parseInt(req.user.total_token) + parseInt(req.body.total_no_of_token);
                    User.findByIdAndUpdate({ _id: req.user._id }, {
                        [coin_bal_name]: coin_bal,
                        [coin_bonus_name]: new_coin_bonus_bal,
                        "total_token": total_token

                    }, function(err, docs) {
                        if (err) res.json(err);
                        else {

                        }
                    });

                    // Update Sold Token In setting Table
                    var sold_coin = parseInt(setting.total_sold_coins) + parseInt(req.body.total_no_of_token);
                    var settingdata = [];

                    Setting.findOneAndUpdate({ slug : "setting" }, //taking setting table id static
                        {
                            total_sold_coins: sold_coin
                        },
                        function(err, setting) {
                            if (err) res.json(err);
                            else {
                                // console.log(setting.btc_price);
                                settingdata.push(setting.btc_price);
                                settingdata.push(setting.eth_price);
                                settingdata.push(setting.usd_price);
                            }
                        });



                    if (req.user.parent != 0) {

                        // Update  token Bal to parent user
                        User.findOne({ _id: req.user.parent }, function(err, ref_user) {
                            //console.log(settingdata);
                            updateReferrarLevel(ref_user._id, ref_user.level, settingdata);
                            if (err) { throw err; } else {
                                var user_level = req.user.level;
                                var refer_level = ref_user.level;
                                var ulevelarr = [];
                        //console.log(user_level+"   "+refer_level);

                                RefRate.findOne({ level: refer_level }, function(err, referlevel) {
                                 //   console.log(referlevel);
                                    if (err) { throw err; } else {
                                        if (user_level != 0) {
                                            RefRate.findOne({ level: user_level }, function(err, userlevel) {
                                              //  console.log(userlevel);
                                                if (err) { throw err; } else {
                                                    //add referal when user have level
                                                    
                                                    var percentage = referlevel.percentage - userlevel.percentage;
                                                    if (percentage > 0) {
                                                        var ref_amount = (req.body.total_amount_1 * percentage) / 100; //ref bonus referrer get
                                                        if (req.body.coin == 'BTC') { var referrer_total_amount = parseFloat(ref_user.btc_bal) + ref_amount; var referrer_bonus = parseFloat(ref_user.btc_bonus_bal) + ref_amount; var total_ref_bonus_amount = parseFloat(req.user.ref_btc_bonus_bal) + ref_amount; } 
                                                        else if (req.body.coin == 'ETH') { var referrer_total_amount = parseFloat(ref_user.eth_bal) + ref_amount; var referrer_bonus = parseFloat(ref_user.eth_bonus_bal) + ref_amount;  var total_ref_bonus_amount = parseFloat(req.user.ref_eth_bonus_bal) + ref_amount;}
                                                        else if (req.body.coin == 'USD') { var referrer_total_amount = parseFloat(ref_user.usd_bal) + ref_amount; var referrer_bonus = parseFloat(ref_user.usd_bonus_bal) + ref_amount;  var total_ref_bonus_amount = parseFloat(req.user.ref_usd_bonus_bal) + ref_amount;}
                                                        else if (req.body.coin == 'EUR') { var referrer_total_amount = parseFloat(ref_user.euro_bal) + ref_amount; var referrer_bonus = parseFloat(ref_user.eur_bonus_bal) + ref_amount;  var total_ref_bonus_amount = parseFloat(req.user.ref_eur_bonus_bal) + ref_amount;} //total amount aftter adding bonus
                                                      
                                                        User.findByIdAndUpdate({ _id: ref_user._id }, {
                                                            [coin_bal_name]: referrer_total_amount,
                                                            [coin_bonus_name]: referrer_bonus,
                                                        }, function(err, docs) {

                                                                var user_email=req.user.email;
                                                          
                                                            if (err) res.json(err);
                                                            else {
                                                                  //console.log(buytoken_id+"akjj1");
                                                               var referral = new  Referral;
                                                               referral.user_id = req.user._id;
                                                               referral.ref_user_id = ref_user._id;
                                                               referral.level = refer_level;
                                                               referral.amount = req.body.total_amount_1;
                                                               referral.earn_amt = ref_amount;
                                                               referral.coin = req.body.coin;
                                                               referral.buytoken_id =1;
                                                                referral.users = req.user;
                                                                referral.save(function(err,buyTokenInserted) {
                                                                if (err) {
                                                                    req.flash('error', 'Please Try again' + err)
                                                                    res.redirect('buy-token');
                                                                }
                                                                else{
                                                                        User.findByIdAndUpdate({ _id: req.user._id }, {
                                                                        [coin_ref_bonus_name]: total_ref_bonus_amount,
                                                                    }, function(err, docs1) 

                                                                    {
                                                                        
                                                                    if (err) res.json(err);
                                                                     else {

                                                                        var ico_currency = req.body.coin;
                                                                        var ico_amount = total_coin;
                                                                            // console.log(path);
                                                                            ejs.renderFile("./views/emails/buy_token.ejs", { tokens:tokens,ico_currency:ico_currency,ico_amount:ico_amount, path: path }, function(err, data) {
                                                                                    var mailOptions = {
                                                                                        from: 'testproject342@gmail.com',
                                                                                        to: user_email,
                                                                                        subject: 'Buy LOOiX',
                                                                                        html: data
                                                                                    };

                                                                                    global.transporter.sendMail(mailOptions, function(error, info) {
                                                                                        if (error) {
                                                                                            //console.log(error);
                                                                                        } else {
                                                                                            //console.log('Email sent: ' + info.response);
                                                                                            req.flash('success', 'Buy Token successfully');
                                                                                            res.redirect('/buy-token');
                                                                                        }
                                                                                    })
                                                                                });

                                                                             // req.flash('success', 'Buy Token successfully');
                                                                             //            res.redirect('/buy-token');
                                                                                


                                                                        
                                                                     }
                                                                 })
                                                            }
                                                            });
                                                               
                                                            }
                                                        });

                                                    } else {

                                                        req.flash('success', 'Buy Token successfully');
                                                        res.redirect('/buy-token');
                                                    }
                                                }
                                            })
                                        } else {
                                            //add referal if user havn't level
                                            var percentage = referlevel.percentage;
                                            var ref_amount = (req.body.total_amount_1 * percentage) / 100; //ref bonus referrer get

                                            if (req.body.coin == 'BTC') { var referrer_total_amount = ref_user.btc_bal + ref_amount; var referrer_bonus = parseFloat(ref_user.btc_bonus_bal) + ref_amount;  var total_ref_bonus_amount = parseFloat(req.user.ref_btc_bonus_bal) + ref_amount;} 
                                            else if (req.body.coin == 'ETH') { var referrer_total_amount = ref_user.eth_bal + ref_amount;  var referrer_bonus = parseFloat(ref_user.eth_bonus_bal) + ref_amount;  var total_ref_bonus_amount = parseFloat(req.user.ref_eth_bonus_bal) + ref_amount;} 
                                            else if (req.body.coin == 'USD') { var referrer_total_amount = ref_user.usd_bal + ref_amount;  var referrer_bonus = parseFloat(ref_user.usd_bonus_bal) + ref_amount;  var total_ref_bonus_amount = parseFloat(req.user.ref_usd_bonus_bal) + ref_amount;} 
                                            else if (req.body.coin == 'EUR') { var referrer_total_amount = ref_user.euro_bal + ref_amount;  var referrer_bonus = parseFloat(ref_user.eur_bonus_bal) + ref_amount;  var total_ref_bonus_amount = parseFloat(req.user.ref_eur_bonus_bal) + ref_amount;} //total amount aftter adding bonus
                                            //console.log(referrer_total_amount);
                                            User.findByIdAndUpdate({ _id: ref_user._id }, {
                                                [coin_bal_name]: referrer_total_amount,
                                                [coin_bonus_name]: referrer_bonus,
                                            }, function(err, docs) {
                                                var user_email=req.user.email;
                                                if (err) res.json(err);
                                                else {
                                                   // console.log(buytoken_id+"akjj2");
                                                               var referral = new  Referral;
                                                               referral.user_id = req.user._id;
                                                               referral.ref_user_id = ref_user._id;
                                                               referral.level = refer_level;
                                                               referral.amount = req.body.total_amount_1;
                                                               referral.earn_amt = ref_amount;
                                                               referral.coin = req.body.coin;
                                                               referral.buytoken_id =1;
                                                                 referral.users = req.user;
                                                               referral.save(function(err,buyTokenInserted) {
                                                                if (err) {
                                                                    req.flash('error', 'Please Try again' + err)
                                                                    res.redirect('buy-token');
                                                                }
                                                                else{
                                                                        User.findByIdAndUpdate({ _id: req.user._id }, {
                                                                        [coin_ref_bonus_name]: total_ref_bonus_amount,
                                                                    }, function(err, docs1) {
                                                                        //console.log(referrer_total_amount);
                                                                    if (err) res.json(err);
                                                                     else {
                                                                          var ico_currency = req.body.coin;
                                                                        var ico_amount = total_coin;
                                                                                 //console.log(path);
                                                                            ejs.renderFile("./views/emails/buy_token.ejs", { tokens:tokens,ico_currency:ico_currency,ico_amount:ico_amount, path: path }, function(err, data) {
                                                                                    var mailOptions = {
                                                                                        from: 'testproject342@gmail.com',
                                                                                        to: user_email,
                                                                                        subject: 'Buy LOOiX',
                                                                                        html: data
                                                                                    };

                                                                                    global.transporter.sendMail(mailOptions, function(error, info) {
                                                                                        if (error) {
                                                                                           // console.log(error);
                                                                                        } else {
                                                                                           // console.log('Email sent: ' + info.response);
                                                                                            req.flash('success', 'Buy Token successfully');
                                                                                            res.redirect('/buy-token');
                                                                                        }
                                                                                    })
                                                                                });

                                                                        // req.flash('success', 'Buy Token successfully');
                                                                        // res.redirect('/buy-token');
                                                                     }
                                                                })
                                                            }
                                                            });
                                                }
                                            });
                                        }
                                    }
                                })

                            }

                        })
                    } else {
                        //console.log(buytoken_id+"akjj3");
                        req.flash('success', 'Buy Token successfully');
                        res.redirect('/buy-token');
                    }

                } else {
                    //console.log("failed");
                    req.flash('error', 'Please Try again !!');
                    res.redirect('/buy-token');
                }

            }
        })
    })

 

    app.get('/token-history', global.isUserAllowed, function(req, res, db) {
        res.locals = {
            title: 'LOOiX | History'
        };
        BuyToken.find({}, function(err, tokens) {
            if (err) { throw err; } else {
                res.render('user/ico/token_history', { data: tokens, user: req.user })
            }
        })

    })

    app.get('/referral', global.isUserAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | History'
        };
        User.find({'parent':req.user._id},function(err,data){
            if(err){
                throw err;
            }
            else
            {   //console.log(data);
                 res.render('user/referrals',{user:req.user,ref:data});
            }
        })
    })
} //module export end


var findreflevel = function(level) {
    //console.log(level);
    RefRate.findOne({ level: level }, function(err, userlevel) {
        if (err) { throw err; } else {
          //  console.log(userlevel);
            return userlevel;
        }
    })
}

var updateReferrarLevel = function(referrar_id, level, settingdata) {
    //console.log("referrar_id"+referrar_id);
    //console.log("btc_price"+settingdata[0]);
    User.find({ 'parent': referrar_id }, function(err, data) {
        if (err) { throw err; } else {
            //console.log(data);
            var total = 0;
            var total_arr = [];
            var max_total = 0;
            for (var i = 0, len = data.length; i < len; i++) {
                //console.log(data[i]['_id']);
                var btcprice = settingdata[0];
                var ethprice = settingdata[1];
                var usdprice = settingdata[2];

                var total_temp = (data[i]['btc_bal'] * btcprice) + (data[i]['eth_bal'] * ethprice) + (data[i]['usd_bal'] * usdprice) + data[i]['euro_bal'];
                // console.log(total_temp);
                total_arr.push(total_temp);
                total = total + total_temp; // take highest investor value 50% only
                if (max_total < total_temp) {
                    max_total = total_temp;
                }
            }
            //console.log("total array"+ total_arr);
            //console.log("total"+total);

            if (level <= 2) {

            } else {
                //var max_total=Math.max(total_arr);
                //console.log("max total: "+max_total);
                total = 0;
                for (var j = 0, jlen = total_arr.length; j < jlen; j++) {
                    //console.log(total_arr[j]);

                    if (total_arr[j] == max_total) {
                        total = total + (total_arr[j] / 2);
                    } else {
                        total = total + total_arr[j];
                    }
                }
            }
            //console.log("total:"+ total);
            var new_ref_level =1;
            if (total >= 15000 && total < 50000) { var new_ref_level = 2; } else if (total >= 50000 && total < 100000) { var new_ref_level = 3; } else if (total >= 100000 && total < 250000) { var new_ref_level = 4; } else if (total >= 250000 && total < 500000) { var new_ref_level = 5; } else if (total == 500000) { var new_ref_level = 6; } else if (total > 500000) { var new_ref_level = 7; }
            //console.log("new_ref_level:" + new_ref_level);
            User.findByIdAndUpdate({ _id: referrar_id }, { level: new_ref_level }, function(err, result) {
                if (err) { throw err; } else {

                    return 0;
                }
            })

        }
    })

}

function randomString() {
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 35;
    var randomstring = '';
    for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}

