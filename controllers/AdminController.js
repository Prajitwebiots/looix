var express = require('express');
var bodyParser = require('body-parser');
var urlencodeParser = bodyParser.urlencoded({ extended: false });
var mongoose = require('mongoose');
var bCrypt = require('bcryptjs');
var validator = require('express-validator');
var multer = require('multer');
var path = require('path');
var passport = require("passport");
const LocalStrategy = require('passport-local').Strategy;
var dbConfig = require('../db.js');
// Model
var User = require('../models/User.js');
var Deposit = require('../models/Deposit.js');
var BuyToken = require('../models/BuyToken.js');
var Setting = require('../models/Setting.js');
var Partner = require('../models/Partner.js');
var RefRate = require('../models/RefRate.js');
var Withdraw = require('../models/Withdraw.js');
var CoinAddress = require('../models/CoinAddress.js');
var Coinpayments = require('coinpayments');

require('./configure.js');
var options = require('./coinpayment_config.js');

var ejs = require("ejs");
var mail = require('../mail.js');

mongoose.connect(dbConfig.url);
    
var client = new Coinpayments(options);


module.exports = function(app) {

    app.get('/admin-dashboard', isAdminAllowed, function(req, res) {
        res.locals = {
           title: 'LOOiX | Dashboard'
        };

        var url = req.protocol + '://' + req.get('host');

        startDate = new Date();
        startDate.setHours(05, 30, 00, 400);

        endDate = new Date();
        endDate.setHours(29, 29, 00, 400);

        var dep_size=0;
        var with_size=0;
        var user_size =0;
        var btc_bal =0;
        var eth_bal = 0;

        Setting.findOne({}, function(err, data) {
            if (err) { throw err; } else {
                Withdraw.find({ created_date: { "$gte": startDate, "$lt": endDate } }).sort({ _id: -1 }).populate('users').exec( function(err, datawithdraw) {
                   BuyToken.find({ created_date: { "$gte": startDate, "$lt": endDate } }).sort({ _id: -1 }).populate('users').exec(  function(err, databuy)  {
                     client.balances(function(err,balances) {

                        if(err) console.log(err);
                        else{
                            console.log(balances);
                            if(balances.BTC){
                                  btc_bal = balances.BTC.balancef;
                            }
                            else if(balances.ETH){
                               eth_bal = balances.ETH.balancef; 
                            }
                    
                            Deposit.find({ created_date: { "$gte": startDate, "$lt": endDate } }).sort({ _id: -1 }).populate('users').exec( function(err, depositdata)  {
                                 Deposit.find({}).populate('users').exec(function(err, ddata) { if (err) { throw err; } else {
                                     var dep_size = ddata.length;
                                        Withdraw.find({}).populate('users').exec(function(err, wdata){ if (err) { throw err; } else {
                                             var with_size = wdata.length;
                                               User.find({}, function(err, udata) { if (err) { throw err; } else {
                                                   var user_size = udata.length;
                                                      if (req.user.role == 1) { res.render('dashboard/dashboard', { user: req.user, url: url, setting: data }); }
                                                      else if (req.user.role == 0) { res.render('admin/dashboard', { dep_size:dep_size,with_size:with_size,user_size:user_size, user: req.user, url: url, btc_bal:btc_bal,eth_bal:eth_bal, setting: data, depdata: depositdata, withdata: datawithdraw, databuy: databuy, }); } else { res.send("no match"); }
                                               }   });
                                        }   });
                                 }   });                
                             })
                         }
                       });
                    })
                })
            }
        })
    });

    app.get('/admin-buy', isAdminAllowed, function(req, res) 
    {

        // BuyToken.aggregate([{
        //     $lookup: {
        //         from: "users", // collection name in db
        //         localField: "user_id",
        //         foreignField: "_id",
        //         as: "users"
        //     }
        // }]).exec(function(err, data) {
        //     console.log(data);
        //     console.log(err);
        //     // students contain WorksnapsTimeEntries
        // });


        res.locals = {
            title: 'LOOiX | Buy History'
        };

       BuyToken.find({}).populate('users')
            .exec(function(err, tokens) {
            if (err) { throw err; } else {
                console.log(tokens);
                res.render('admin/transaction/buy', { user: req.user, data: tokens, success: req.flash('success'), error: req.flash('error') });
            }
        });
    });

    app.get('/admin-partner', isAdminAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Manage Partner'
        };
        Partner.find({}, function(err, partners) {
            if (err) { throw err; } else {
                res.render('admin/partners/index', { user: req.user, data: partners, success: req.flash('success'), error: req.flash('error') });
            }
        })
    });



    app.get('/partner-add', isAdminAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | ADD Partner'
        };
        res.render('admin/partners/save', { user: req.user, success: req.flash('success'), error: req.flash('error') });
    });


    app.get('/admin-profile', isAdminAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Profile'
        };
        res.render('admin/profile', { user: req.user, success: req.flash('success'), error: req.flash('error') });
    });

    app.post('/admin-update-profile', urlencodeParser, function(req, res) {
        User.findByIdAndUpdate({ _id: req.body.user_id }, {
            firstname: req.body.firstname,
            lastname: req.body.lastname
        }, function(err, docs) {
            if (err) res.send(err);
            else {
                // console.log(docs);
                req.flash('success', 'Profile updated successfully.');
                res.redirect('/admin-profile');
            }
        });
    })


    app.post('/admin-changepass', urlencodeParser, isAdminAllowed, function(req, res, next) {
        var password = req.body.old_password;
        if (!isValidPassword(req.user, password)) {
            req.flash('error', 'Invalid Old Password');
            res.redirect('/admin-profile');
        } else {
            if (req.body.new_password !== req.body.confirm_password) {
                //throw new Error('password and confirm password do not match');
                req.flash('error', 'password and confirm password do not match.');
                res.redirect('/admin-profile');
            } else {
                var user = req.user;
                user.password = createHash(req.body.new_password);
                user.save(function(err) {
                    if (err) { next(err) } else {
                        req.flash('success', 'Password updated successfully.');
                        res.redirect('/admin-profile');
                    }
                })
            }
        }
    });


    app.post('/admin-edit-profile', urlencodeParser, isAdminAllowed, function(req, res, next) {

        var upload = multer({
            storage: storage,
            fileFilter: function(req, file, callback) {
                var ext = path.extname(file.originalname)
                if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
                    return callback(res.end('Only images are allowed'), null)
                }
                callback(null, true)
            }
        }).single('photo');
        upload(req, res, function(err) {
            if (err) {
                req.flash('error', 'Oops! profile not uploaded.');
                res.redirect('/admin-profile');
            } else {
                User.findByIdAndUpdate({ _id: req.user._id }, {
                    profile_pic: req.file.filename
                }, function(err, docs) {
                    if (err) res.send(err);
                    else {
                        //console.log(docs);
                        req.flash('success', 'Profile is uploaded.');
                        res.redirect('/admin-profile');
                    }
                });
            }
        })
    })



    app.post('/partner-save', urlencodeParser, isAdminAllowed, function(req, res, next) {

        var upload = multer({
            storage: storage_partner,
            fileFilter: function(req, file, callback) {
                var ext = path.extname(file.originalname)
                if (ext !== '.png' && ext !== '.jpg' && ext !== '.gif' && ext !== '.jpeg') {
                    return callback(res.end('Only images are allowed'), null)
                }
                callback(null, true)
            }
        }).single('photo');
        upload(req, res, function(err) {
            if (err) {
                req.flash('error', 'Oops! Partner Image not uploaded.');
                res.redirect('/admin-partner');
            } else {
                var part = new Partner();
                part.partner_image = req.file.filename;
                part.save(function(err, data) {
                    if (err) res.send(err);
                    else {
                        req.flash('success', 'Partner is uploaded.');
                        res.redirect('/admin-partner');
                    }
                });
            }
        })
    })


    app.get('/admin-kyc', isAdminAllowed, function(req, res) {

        res.locals = {
            title: 'LOOiX | KYC'
        };
        User.find({ role: 1, kyc_backend: { $ne: '' } }, function(err, userdata) {
            if (err) { throw err; } else {
                res.render('admin/kyc/index', { 'message': req.flash('message'), 'success': req.flash('success'), 'error': req.flash('error'), user: req.user, data: userdata })
            }
        })
    })


    app.get('/kyc-approve/:id', function(req, res) {
        User.findByIdAndUpdate({ _id: req.params.id }, {
            kyc_status: 1
        }, function(err, docs) {
            if (err) res.send(err);
            else {
                req.flash('success', ' KYC approved Successfully.');
                res.redirect('/admin-kyc');
            }
        });
    });
    app.get('/kyc-reject/:id', function(req, res) {
        User.findByIdAndUpdate({ _id: req.params.id }, {
            kyc_status: 2
        }, function(err, docs) {
            if (err) res.send(err);
            else {
                req.flash('success', ' KYC rejected Successfully.');
                res.redirect('/admin-kyc');
            }
        });
    });



    app.get('/admin-setting', isAdminAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Setting'
        };

        Setting.find({}, function(err, settingdata) {
            if (err) { throw err; } else {
                res.render('admin/setting/index', { 'message': req.flash('message'), 'success': req.flash('success'), 'error': req.flash('error'), user: req.user, data: settingdata })
            }
        })
    })

    app.get('/admin-setting-show/:id', isAdminAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Setting Edit'
        };
        Setting.findOne({ _id: req.params.id }, function(err, settingdata) {
            if (err) { throw err; } else {
                res.render('admin/setting/edit', { 'message': req.flash('message'), 'success': req.flash('success'), 'error': req.flash('error'), user: req.user, data: settingdata })
            }
        })
    })


    app.post('/setting-edit', isAdminAllowed, urlencodeParser, function(req, res) {
        
        Setting.findByIdAndUpdate({ _id: req.body.setting_id }, {
            email: req.body.email,
            ico_start_date: req.body.ico_start_date,
            ico_end_date: req.body.ico_end_date,
            total_coins: req.body.total_coins,
            rate: req.body.rate,
            bonus: req.body.bonus,
            private_key: req.body.private_key,
            public_key: req.body.public_key,

        }, function(err, docs) {
            if (err) res.send(err);
            else {
                req.flash('success', ' Setting Edit Successfully.');
                res.redirect('/admin-setting');
            }
        });
       
    })

    app.post('/rate-edit', isAdminAllowed, urlencodeParser, function(req, res) 
    {   
        RefRate.findByIdAndUpdate({ _id: req.body.rate_id }, {
            percentage: req.body.percentage,
        }, function(err, docs) {
            if (err) res.send(err);
            else {
                req.flash('success', ' Rate Edit Successfully.');
                res.redirect('/admin-rate');
            }
        });
    })


    app.get('/admin-users', isAdminAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Users'
        };

        User.find({}).sort({ _id: -1 }).exec(function(err, userdata) {
            if (err) { throw err; } else {
                res.render('admin/users', { 'message': req.flash('message'), 'success': req.flash('success'), 'error': req.flash('error'), user: req.user, data: userdata })
            }
        })
    })

    app.get('/admin-rate', function(req, res) {
        res.locals = {
            title: 'LOOiX | Admin Rate'
        };
        RefRate.find({}, function(err, ratedata) {
            if (err) { throw err; } else {
                res.render('admin/rate/index', { 'message': req.flash('message'), 'success': req.flash('success'), 'error': req.flash('error'), user: req.user, data: ratedata })
            }
        })
    })


    app.get('/admin-block/:id', function(req, res) {
        User.findByIdAndUpdate({ _id: req.params.id }, {
            is_active: 1
        }, function(err, docs) 
        {
           var user_email=docs.email;
           var username=docs.username;
            if (err) res.send(err);
            else 
            {
                      ejs.renderFile("./views/emails/user_status.ejs", { username:username,status:"Block" }, function(err, data) {
                                            var mailOptions = {
                                                from: 'testproject342@gmail.com',
                                                to: user_email,
                                                subject: 'User Status',
                                                html: data
                                            };

                                            global.transporter.sendMail(mailOptions, function(error, info) {
                                                if (error) {
                                                    console.log(error);
                                                } else {
                                                      console.log('Email sent: ' + info.response);
                                                      req.flash('success', ' User Blocked Successfully.');
                                                      res.redirect('/admin-users');
                                                }
                                            })
                                        });
            }
        });
    })

    app.get('/admin-active/:id', function(req, res) {
        User.findByIdAndUpdate({ _id: req.params.id }, {
            is_active: 0
        }, function(err, docs) {

              var user_email=docs.email;
             var username=docs.username;
            if (err) res.send(err);
            else {

            ejs.renderFile("./views/emails/user_status.ejs", { username:username,status:"Active" }, function(err, data) {
                                            var mailOptions = {
                                                from: 'testproject342@gmail.com',
                                                to: user_email,
                                                subject: 'User Status',
                                                html: data
                                            };

                                            global.transporter.sendMail(mailOptions, function(error, info) {
                                                if (error) {
                                                    console.log(error);
                                                } else {
                                                      console.log('Email sent: ' + info.response);
                                                       req.flash('success', ' User Activate Successfully.');
                                                       res.redirect('/admin-users');
                                                }
                                            })
                                        });
            }
        });
    })


    app.get('/rate-edit-one/:id', isAdminAllowed, function(req, res) {
        res.locals = {
            title: 'LOOiX | Edit Rate'
        };
        RefRate.findOne({ _id: req.params.id }, function(err, ratedata) {
            if (err) { throw err; } else {
                res.render('admin/rate/edit', { 'message': req.flash('message'), 'success': req.flash('success'), 'error': req.flash('error'), user: req.user, data: ratedata })
            }
        })
    })


    app.get('/admin-delete/:id', function(req, res) {
        User.findByIdAndUpdate({ _id: req.params.id }, {
            is_deleted: 1
        }, function(err, docs) {
            var user_email=docs.email;
           var username=docs.username;

            if (err) res.send(err);
            else {

                ejs.renderFile("./views/emails/user_status.ejs", { username:username,status:"Delete" }, function(err, data) {
                                            var mailOptions = {
                                                from: 'testproject342@gmail.com',
                                                to: user_email,
                                                subject: 'User Status',
                                                html: data
                                            };

                                            global.transporter.sendMail(mailOptions, function(error, info) {
                                                if (error) {
                                                    console.log(error);
                                                } else {
                                                      console.log('Email sent: ' + info.response);
                                                       req.flash('success', ' User Delete Successfully.');
                                                       res.redirect('/admin-users');
                                                }
                                            })
                                        });
             
            }
        });
    });
    app.get('/partner-delete/:id', function(req, res) {
        Partner.remove({ _id: req.params.id }, function(err, prod) {
            if (err) {
                req.flash('error', 'Something Wrong to Remove Partner' + err);
                res.redirect('/admin-partner');
            } else {
                req.flash('error', ' Partner Remove Successfully');
                res.redirect('/admin-partner');
            }
        })
    })


}; //module end

var isValidPassword = function(user, password) {
    return bCrypt.compareSync(password, user.password);
}
var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}
var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/assets/images/user')
    },
    filename: function(req, file, callback) {
        callback(null, req.user.username + '-' + Date.now() + path.extname(file.originalname))
    }
})



var storage_kyc = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/assets/images/kyc')
    },
    filename: function(req, file, callback) {
        callback(null, req.user._id + '-' + Date.now() + path.extname(file.originalname))
    }
})

var storage_partner = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, './public/assets/images/partner')
    },
    filename: function(req, file, callback) {
        callback(null, req.user._id + '-' + Date.now() + path.extname(file.originalname))
    }
})