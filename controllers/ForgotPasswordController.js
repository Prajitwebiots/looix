var bodyParser = require('body-parser');
var urlencodeParser = bodyParser.urlencoded({ extended: false });
var mongoose = require('mongoose');
var async = require('async'); //to avoid dealing with nested callbacks
var bCrypt = require('bcryptjs');
var crypto = require('crypto'); //generating random token 
var User = require('../models/User.js');
var ejs = require("ejs");
var mail = require('../mail.js');
module.exports = function(app) {   

    app.get('/forgotPassword', function(req, res) {
        res.locals = {
            title: 'LOOiX | forgot Password'
        };
        res.render('auth/forgotpassword', { 'error': req.flash('error'), 'success': req.flash('success') });
    });

    app.post('/forgotPassword', urlencodeParser, function(req, res, next) {
        async.waterfall([
            function(done) {
                crypto.randomBytes(20, function(err, buf) {
                    var token = buf.toString('hex');
                    done(err, token);
                });
            },
            function(token, done) {
                User.findOne({ email: req.body.email }, function(err, user) {
                    if (!user) {
                        req.flash('error', 'No account with that email address exists.');
                        return res.redirect('/forgotPassword');
                    }
                    user.resetPasswordToken = token;
                    user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                    user.save(function(err) {
                        var path = req.protocol + '://' + req.get('host');
                        var link = path + '/reset/' + token;
                        ejs.renderFile("./views/emails/forgotpassword.ejs", { username: user.username, email: req.body.email, link: link, path: path }, function(err, data) {
                            var mailOptions = {
                                from: 'testproject342@gmail.com',
                                to: req.body.email,
                                subject: 'Reset Password',
                                html: data
                            };

                            global.transporter.sendMail(mailOptions, function(error, info) {
                                if (error) {
                                    console.log(error);
                                } else {
                                    console.log('Email sent: ' + info.response);
                                }
                            })
                        });

                        req.flash('success', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
                        done(err, token, user);
                    });
                });
            }





        ], function(err) {
            if (err) return next(err);
            res.redirect('/forgotPassword');
        });
    });

    app.get('/reset/:token', function(req, res) {
        res.locals = {
            title: 'LOOiX | Reset Password'
        };
        User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
            if (!user) {
                req.flash('error', 'Password reset token is invalid or has expired.');
                return res.redirect('/forgotPassword');
            }
            res.render('auth/reset', { 'error': req.flash('error'), 'success': req.flash('success'), user: req.user, token: req.params.token });
        });
    });

    app.post('/reset/:token', urlencodeParser, function(req, res) {
        if (req.body.password == req.body.confirm) {
            async.waterfall([
                function(done) {
                    User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
                        console.log(user);
                        if (!user) {
                            req.flash('error', 'Password reset token is invalid or has expired.');
                            return res.redirect('back');
                        }

                        user.password = createHash(req.body.password);
                        user.resetPasswordToken = '';
                        user.resetPasswordExpires = '';

                        user.save(function(err) {
                            // req.logIn(user, function(err) {
                            req.flash('success', 'Success! Your password has been changed.');
                            res.redirect('/login');
                            //done(err, user);
                            // });
                        });
                    });
                }

            ], function(err) {
                res.redirect('/');
            });
        } else {
            req.flash('error', 'Password and confirm password do not match');
            return res.redirect('back');
        }
    });
}

var createHash = function(password) {
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}