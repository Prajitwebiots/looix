var express = require('express');
var bodyParser = require('body-parser');
var validator = require('express-validator');
var app = express();
var mongoose = require('mongoose');
var bCrypt = require('bcryptjs');
//var validator = require('express-validator');
var dbConfig = require('../db.js');
var ejs = require("ejs");
var mail = require('../mail.js');


//Model
var User = require('../models/User.js');
var Login = require('../models/Login.js');

var passport = require("passport");
const LocalStrategy = require('passport-local').Strategy;
mongoose.connect(dbConfig.url);
var urlencodeParser = bodyParser.urlencoded({ extended: false });

app.use(urlencodeParser);
app.use(validator());

var Setting = require('../models/Setting.js');
var Deposit = require('../models/Deposit.js');
passport.serializeUser(function(user, done) {
    done(null, user._id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});


module.exports = function(app) {


    // app.get('/testCreate',function(req,res){
    //    // fs.mkdir('./public/logs', ["rw"], function(err) {});
    //    // fs.mkdirSync('./public/logs', ["rw"]);
    //    // fs.writeFile('./public/logs/test.txt', 'aaa', function(err) {})
    //    var fileContent = "Hello World!";
    //    var filepath = "./public/logs/mynewfile.txt";
    //    fs.writeFile(filepath, fileContent, (err) => {
    //        if (err) throw err;
    //        console.log("The file was succesfully saved!");
    //    }); 
    // })

    
    app.get('/testing', function(req, res) 
        {
            console.log('ddfd');
       });


    app.get('/register', function(req, res) {

        if (req.user) {
            res.redirect('/');
        } else {
            res.locals = {
                title: 'LOOiX | Register',
            };

            res.render('auth/register', { 'message': req.flash('message'), 'success': req.flash('success'), 'error': req.flash('error') });
        }
    });

    app.get('/activation/:email/:token', function(req, res) {

        User.findOne({ 'email': req.params.email, 'activationToken': req.params.token },
            function(err, user) {
                if (user) {
                    User.findByIdAndUpdate({ _id: user._id }, {
                        is_active: '0',
                    }, function(err, success) {
                        if (err) res.send(err);
                        else {
                            req.flash('success', 'Your account is activated, Now you can access your account.');
                            res.redirect('/login');
                        }
                    });
                } else {
                    req.flash('message', 'User Not found.');
                    res.redirect('/login');
                }
            });
    });

    app.get('/refer/:token', function(req, res) {
        sess = req.session;
        sess.referral_token = req.params.token;

        res.locals = {
            title: 'LOOiX | Register',
        };
        res.render('auth/register', { 'message': req.flash('message'), 'success': req.flash('success'), 'error': req.flash('error') });
        //res.redirect('/register');


    });

    app.post('/register', function(req, res, next) {

            req.checkBody('firstname', 'First Name is required').notEmpty();
            req.checkBody('lastname', 'Last Name is required').notEmpty();
            req.checkBody('email', 'Email is not valid').isEmail();
            req.checkBody('username', 'Username is required').notEmpty();
            req.checkBody('password', 'Password is required').notEmpty();
            req.checkBody('confirm_password', 'Confirm Password is required').notEmpty();
            req.checkBody('confirm_password', 'Password dont match').equals(req.body.password);
            req.checkBody('erc20_address', 'ERC20 Address is required').notEmpty();

            var error = req.validationErrors();
            if (error) {
                res.render('auth/register', { 'title': 'LOOiX | Register', 'message': req.flash('message'), 'success': req.flash('success'), 'error': error });
            } else {
                var isaddress = isAddress(req.body.erc20_address);
                if (isaddress == true) {
                    User.findOne({ 'erc20_address': req.body.erc20_address }, function(err, user) {

                        if (user) {
                            req.flash('message', 'This ERC20 address already exist.');
                            res.render('auth/register', { 'title': 'LOOiX | Register', 'message': req.flash('message'), 'success': req.flash('success'), 'error': error });
                        } else {
                            return next();
                        }
                    });
                } else {
                    req.flash('message', 'Please enter valid ERC20 addrss.');
                    res.render('auth/register', { 'title': 'LOOiX | Register', 'message': req.flash('message'), 'success': req.flash('success'), 'error': error });
                }
            }
        },
        passport.authenticate('signup', {
            successRedirect: '/',
            failureRedirect: '/register',
            failureFlash: true
        }));

    app.get('/check-address/:address', function(req, res) {
        var address = req.params.address;
        var isaddress = isAddress(address);
        res.send(isaddress);

    })

    app.get('/login', function(req, res) {

        Global();
        req.logout();
        if (req.user) {
            res.redirect('/');
        } else {
            res.locals = {
                title: 'LOOiX | Login'
            };
            res.render('auth/login', { 'message': req.flash('message'), 'success': req.flash('success'), 'error': req.flash('error') });
        }
    });






    app.post('/login', function(req, res, next) {
            req.checkBody('email', 'Email is not valid').isEmail();
            req.checkBody('password', 'Password is required').notEmpty();
            var error = req.validationErrors();
            if (error) {
                res.render('auth/login', { 'title': 'LOOiX | Login', 'message': req.flash('message'), 'success': req.flash('success'), 'error': error });
            } else {

                return next();
            }
        },
        passport.authenticate('login', {
            successRedirect: '/dashboard',
            failureRedirect: '/login',
            failureFlash: true
        }));


    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/login');
    });

    /****************************Login with passport*************************/

    passport.use('login', new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, email, password, done) {


            // check in mongo if a user with username exists or not
            User.findOne({ 'email': email },
                function(err, user) {


                    // In case of any error, return using the done method
                    if (err) {
                        return done(err);
                    }
                    if (user) {
                        if (user.is_active == 1) {
                            return done(null, false, req.flash('message', 'Your account is not active.'));
                        }
                    }
                    // Username does not exist, log error & redirect back
                    if (!user) {
                        return done(null, false, req.flash('message', 'User Not found.'));
                    }

                    // User exists but wrong password, log the error 
                    if (!isValidPassword(user, password)) {
                        return done(null, false, req.flash('message', 'Invalid Username and Password'));
                    }

                    var slug = "";
                    var userid = 0;

                    if (user) {
                        if (user.role == 1) {
                            slug = "user";
                            userid = user._id;
                        } else if (user.role == 0) {
                            slug = "admin";
                            userid = user._id;
                        } else {}
                    }


                    var ip = req.connection.remoteAddress;

                    var login = new Login();

                    login.user_id = userid;
                    login.ip_address = ip;
                    login.slug = slug;
                    login.created_date = new Date();
                      login.users = req.user;
                    login.save(function(err) {
                        if (err) throw err;
                        else {
                            // done method which will be treated like success
                            return done(null, user);


                        }
                    });

                }
            );
        }));

    var isValidPassword = function(user, password) {
        return bCrypt.compareSync(password, user.password);
    }

    /****************************Signup with passport*************************/



    passport.use('signup', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, username, password, done, res) {

            findOrCreateUser = function() {

                User.findOne({ 'username': username }, function(err, user) {

                    // already exists
                    if (user) {
                        return done(null, false, req.flash('message', 'User Already Exists'));
                    } else {
                        var referrer_id = 0;
                        var ref_token = randomString(20);
                        var activationToken = randomString(20);
                        sess = req.session;
                        var referral_token = sess.referral_token;
                        var trade_account_no = randomString(6);
                        // console.log(ref_token);
                        if (referral_token) {
                            User.findOne({ 'ref_token': referral_token }, function(err, refuser) {
                                if (refuser) {
                                    //add referral
                                    var referrer_id = refuser._id; 
                                     console.log(referrer_id);
                                    var newUser = new User();
                                    // set the user's local credentials
                                    newUser.username = username;
                                    newUser.password = createHash(password);
                                    newUser.email = req.param('email');
                                    newUser.firstname = req.param('firstname');
                                    newUser.lastname = req.param('lastname');
                                    newUser.parent = referrer_id;
                                    newUser.ref_token = ref_token;
                                    newUser.trade_account_no = trade_account_no;
                                    newUser.erc20_address = req.param('erc20_address');

                                    // save the user
                                    newUser.save(function(err) {
                                        if (err) {
                                            console.log('Error in Saving user: ' + err);
                                          return done(null, false, req.flash('message', err));
                                        }
                                        updateRefUserLevel(referrer_id);

                                        var path = req.protocol + '://' + req.get('host');

                                        var link = path + '/activation/' + req.param('email') + '/' + activationToken;

                                        ejs.renderFile("./views/emails/activation.ejs", { username: req.param('username'), link: link, path: path }, function(err, data) {
                                            var mailOptions = {
                                                from: 'testproject342@gmail.com',
                                                to: req.param('email'),
                                                subject: 'Account Activation',
                                                html: data
                                            };

                                            global.transporter.sendMail(mailOptions, function(error, info) {
                                                if (error) {
                                                    console.log(error);
                                                } else {
                                                    console.log('Email sent: ' + info.response);
                                                }
                                            })
                                        });

                                        return done(null, false,
                                            req.flash('success', 'Thank you. To complete your registration please check your email.'));
                                    });
                                }
                                else{
                                        return done(null, false, req.flash('message', 'Wrong referral link'));
                                }
                            })

                        } else {

                              //console.log(referrer_id);
                            var newUser = new User();
                            // set the user's local credentials
                            newUser.username = username;
                            newUser.password = createHash(password);
                            newUser.email = req.param('email');
                            newUser.firstname = req.param('firstname');
                            newUser.lastname = req.param('lastname');
                            newUser.parent = referrer_id;
                            newUser.ref_token = ref_token;
                            newUser.activationToken = activationToken;
                            newUser.trade_account_no = trade_account_no;
                            newUser.erc20_address = req.param('erc20_address');
                            // save the user
                            newUser.save(function(err) {
                                if (err) {
                                    // console.log('Error in Saving user: ' + err);
                                    throw err;
                                }

                                var path = req.protocol + '://' + req.get('host');

                                var link = path + '/activation/' + req.param('email') + '/' + activationToken;

                                ejs.renderFile("./views/emails/activation.ejs", { username: req.param('username'), link: link, path: path }, function(err, data) {
                                    var mailOptions = {
                                        from: 'testproject342@gmail.com',
                                        to: req.param('email'),
                                        subject: 'Account Activation',
                                        html: data
                                    };

                                    global.transporter.sendMail(mailOptions, function(error, info) {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            console.log('Email sent: ' + info.response);
                                        }
                                    })
                                });


                                return done(null, false,
                                    req.flash('success', 'Thank you. To complete your registration please check your email.'));
                            });
                        }
                    }
                });
            };

            process.nextTick(findOrCreateUser);
        }));





    // Generates hash using bCrypt
    var createHash = function(password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

    function randomString(length) {
        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = length;
        var randomstring = '';
        for (var i = 0; i < length; i++) {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1);
        }
        return randomstring;
    }

};
global.isAddress = function(address) {
    var myregexp = /^(0x)?[0-9a-f]{40}$/i;
    var match = myregexp.exec(address);
    //console.log(match+"match");
    if (match == null) {
        return false;
    } else {
        var myregexp1 = /^(0x)?[0-9a-f]{40}$/;
        var match1 = myregexp1.exec(address);
        var myregexp2 = /^(0x)?[0-9a-fA-F]{40}$/;
        var match2 = myregexp2.exec(address);
        if (match1 == null || match2 != null) {
            return true;
        }
    }
}

function updateRefUserLevel(referrer_id) {
    User.findOne({ '_id': referrer_id }, function(err, data) {
        if (data.level <= 0) {
            User.findByIdAndUpdate({ _id: referrer_id }, {
                level: 1
            }, function(err, docs) {

            })
        }
    })
}

function Global() {
    Setting.findOne({}, function(err, data) {
        if (err) { throw err; } else {
            global.btc_price = data.btc_price;
            global.eth_price = data.eth_price;
            global.usd_price = data.usd_price;
            global.rate = data.rate;
        }
    });
}
global.isAdminAllowed = function(req, res, next) {

    global.url = req.protocol + '://' + req.get('host');

    sess = req.session;
    if (sess.is_2fa_otp_validate == 1) {
        res.redirect('/login');
    } else {
        if (req.user && req.user.role == 0) {
            return next();
        } else if (req.user && req.user.role == 1) {
            res.redirect('/dashboard');
        } else {
            res.redirect('/login');
        }
    }
};



global.isUserAllowed = function(req, res, next) {
    global.url = req.protocol + '://' + req.get('host');

    sess = req.session;
    if (sess.is_2fa_otp_validate == 1) {
        res.redirect('/login');
    } else {
        if (req.user && req.user.role == 1) {
            return next();
        } else if (req.user && req.user.role == 0) {
            res.redirect('/admin-dashboard');
        } else {
            res.redirect('/login');
        }
    }
}