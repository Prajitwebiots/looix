
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// User Schema
var RefRateSchema = mongoose.Schema({
  level : {  	type:Number ,  	  default:'0'  	},
  percentage:{ type:Number ,      default:'0' }
});

module.exports = mongoose.model('RefRate', RefRateSchema);

