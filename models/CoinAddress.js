var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;
// User Schema
var CoinAddressSchema = mongoose.Schema({
  user_id : {    type:String    },
	coin: {   type: String,   default:''  },
  address: {		type: String,		default:'' 	},
  created_date:{ type:Date ,      default: Date.now },//enter Created date
  users:{type:Schema.Types.String, ref:'User'}
});

module.exports = mongoose.model('CoinAddress', CoinAddressSchema);

