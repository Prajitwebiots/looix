
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;
// User Schema
var LoginSchema = mongoose.Schema({
	  user_id : {  	type:String },
	  ip_address:{ type:String },
	  slug: { type:String },
	 created_date:{ type:Date ,      default: Date.now },//enter Created date
	 users:{type:Schema.Types.String, ref:'User'}
});

module.exports = mongoose.model('Login', LoginSchema);

