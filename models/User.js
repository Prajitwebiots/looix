var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var validator = require('node-mongoose-validator');



// User Schema 
var UserSchema = mongoose.Schema({

    username: { type: String, required: true, index: true },
    password: { type: String, required: true, },
    email: { type: String, required: true, unique: true },
    firstname: { type: String },
    lastname: { type: String },
    resetPasswordToken: { type: String, default: '' },
    resetPasswordExpires: { type: Date, default: '' },
    isset_2fa: { type: Number, default: '0' },
    g_2fa: { type: String, default: '' },
    g_2fa_base32: { type: String, default: '' },
    role: { type: Number, default: '1' },
    total_token: { type: Number, default: '0' },
    token_bonus: { type: Number, default: '0' },
    btc_bal: { type: Number, default: '0' },
    eth_bal: { type: Number, default: '0' },
    usd_bal: { type: Number, default: '0' },
    euro_bal: { type: Number, default: '0' },
    parent: { type: String, default: '0' },
    level: { type: Number, default: '0' },
    kyc_status: { type: Number, default: '0' },
    kyc_front: { type: String, default: '' },
    kyc_backend: { type: String, default: '' },
    kyc_selfie: { type: String, default: '' },
    ref_token: { type: String, default: '' },
    btc_bonus_bal: { type: Number, default: '0' },
    eth_bonus_bal: { type: Number, default: '0' },
    usd_bonus_bal: { type: Number, default: '0' },
    eur_bonus_bal: { type: Number, default: '0' },
    ref_btc_bonus_bal: { type: Number, default: '0' },
    ref_eth_bonus_bal: { type: Number, default: '0' },
    ref_usd_bonus_bal: { type: Number, default: '0' },
    ref_eur_bonus_bal: { type: Number, default: '0' },
    trade_account_no: { type: String, default: '' },
    profile_pic: { type: String, default: '' },
    erc20_address: { type: String, default: '' },
    paypal_email: { type: String, default: '' },
    activationToken: { type: String, default: '' },
    is_active: { type: Number, default: '1' }, //0=active,1= deactive
    is_deleted: { type: Number, default: '0' },
    created_date: { type: Date, default: Date.now }, //enter Created date
});


// Validations 
UserSchema.path('username').validate(validator.notEmpty(), 'Please provide a username.');
UserSchema.path('email').validate(validator.isEmail(), 'Please provide a valid email address');

module.exports = mongoose.model('User', UserSchema);