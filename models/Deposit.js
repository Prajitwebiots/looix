var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var User = require('./User.js');

// User Schema
var DepositSchema = mongoose.Schema({

  user_id : {  	type:String ,  	  default:''  	},
  amount:{ type:Number ,      default:'0' },
  currency:{ type:String ,      default:'' },
  address:{ type:String ,      default:'' },
  payer_id:{ type:String ,      default:'' },
  payment_id:{ type:String ,      default:'' },
  payment_token:{ type:String ,      default:'' },
  tx_id:{ type:String ,      default:'' },
  order_id:{ type:String ,      default:'' },
  status:{ type:Number ,      default:'0' },//0=pending,1=approval,2=cancel 
  admin_status:{ type:Number ,      default:'0' },//0=pending,1=approval,2=cancel
  created_date:{ type:Date ,      default: Date.now },//enter Created date
  userdata    : [{type: Schema.Types.ObjectId, ref: 'User' }],
  users:{type:Schema.Types.String, ref:'User'}
  
});




module.exports = mongoose.model('Deposit', DepositSchema);