var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;
// User Schema
var ReferralSchema = mongoose.Schema({
  user_id : {  	type:String    }, //logged in user
  ref_user_id:{ type:String  }, //referrar user
  level:{ type:Number ,      default:'1' },
  amount:{ type:Number }, //total amount
  earn_amt:{ type:Number },//get amount
  coin : { type: String}, //coin type
  buytoken_id:{ type:String }, //buytoken table id
  created_date:{ type:Date ,      default: Date.now },//enter Created date
  users:{type:Schema.Types.String, ref:'User'}
});

module.exports = mongoose.model('Referral', ReferralSchema);

