var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// User Schema
var SettingSchema = mongoose.Schema({
  email : { type:String },
  ico_start_date:{ type:Date  },
  ico_end_date:{ type:Date },
  total_coins:{ type:Number },
  total_sold_coins:{ type:Number },
  rate:{ type:Number   },
  bonus:{ type:Number },
  ref_percentage:{ type:Number },
  private_key:{ type:String  },
  public_key:{ type:String },
  btc_price:{ type:Number },
  eth_price:{ type:Number },
  euro_price:{ type:Number },
  usd_price:{ type:Number },
  transfer:{ type:Number  },
  slug: {type:String}
 
});

module.exports = mongoose.model('Setting', SettingSchema);

