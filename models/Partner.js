
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

// User Schema
var PartnerSchema = mongoose.Schema({
	  partner_image: { type:String },
	  created_date:{ type:Date ,      default: Date.now },//enter Created date
});

module.exports = mongoose.model('Partner', PartnerSchema);

