var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var Schema = mongoose.Schema;

// User Schema
var BuyTokenSchema = mongoose.Schema({
    user_id: { type: String, default: '1' },
    ref_user_id: { type: String, default: '0' },
    tx_id: { type: String, default: '' },
    amount: { type: Number, default: '0' },
    tokens: { type: Number, default: '0' },
    type: { type: String, default: '0' },

    created_date:{ type:Date ,      default: Date.now },//enter Created date
    users:{type:Schema.Types.String, ref:'User'}
});


module.exports = mongoose.model('BuyToken', BuyTokenSchema);