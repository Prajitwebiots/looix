
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// User Schema
var WithdrawSchema = mongoose.Schema({
  user_id : {  	type:String ,  	  default:''  	},
  amount:{ type:Number ,      default:'0' },
  currency:{ type:String ,      default:'' },
  address:{ type:String ,      default:'' },
  tx_id:{ type:String ,      default:'' },//callback_id for coins
  status:{ type:Number ,      default:'0' },//0=pending,1=approval,2=cancel,3=fail from coinpayment
  payout_batch_id:{type:String, default:''},//for fiat currency
  created_date:{ type:Date ,      default: Date.now },//enter Created date
  users:{type:Schema.Types.String, ref:'User'}
});

module.exports = mongoose.model('Withdraw', WithdrawSchema);

