var express = require('express');
var bodyParser = require('body-parser');
var validator = require('express-validator');
var app = express();
var path = require('path');
var expressLayouts = require('express-ejs-layouts');
var session = require('express-session');
var flash = require('connect-flash');
var dbConfig = require('./db.js');
var mongoose = require('mongoose');
var bCrypt = require('bcryptjs');
var passport = require("passport");
var cron = require('node-cron');
const request = require('request');
var i18n=require("i18n-express");

const LocalStrategy = require('passport-local').Strategy;
mongoose.connect(dbConfig.url);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(validator());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout extractScripts', true)
app.set('layout extractStyles', true)
app.use(expressLayouts);


app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(session({ resave: false, saveUninitialized: true, secret: 'nodedemo' }));
app.use(flash());

//language translation 
app.use(i18n({
  translationsPath: path.join(__dirname, 'lang'), // <--- use here. Specify translations files path.
  siteLangs: ["en","de"],
  textsVarName: 'translation'
}));


//********************Controllers****************
var UserController = require('./controllers/UserController'); // Controller
var AuthController = require('./controllers/AuthController');
var G2faController = require('./controllers/G2faController');
var IcoController = require('./controllers/IcoController');
var WalletController = require('./controllers/WalletController');
var ForgotPasswordController = require('./controllers/ForgotPasswordController');
var GeneralController = require('./controllers/GeneralController');
var AdminController = require('./controllers/AdminController');

UserController(app); //Pass App to user controller
AuthController(app); //Pass App to login controller
G2faController(app); //Pass App to google authenticater controller
IcoController(app);
WalletController(app);
ForgotPasswordController(app);
GeneralController(app);
AdminController(app);

//********************models****************

var Setting = require('./models/Setting.js');
//app.use('/public', express.static('public')); //creating middleware for assets

app.use('/public', express.static(path.join(__dirname, 'public')))

app.get('/', function(req, res) {
    res.locals = {
        title: 'LOOiX | Home'
    };
    if (req.user) {
        var user_loggedin = 1;
    } else {
        var user_loggedin = 0;
    }

    Setting.findOne({}, function(err, data) 
    {
        if (err) { throw err; } else {
            res.render('home/home', { is_loggedin: user_loggedin, setting: data });
        }
    });
});

cron.schedule('*/1 * * * *', function() {
    request('https://www.bitstamp.net/api/v2/ticker/btceur', (error, response, body) => {
        // parse the json answer and get the current bitcoin value
        const data = JSON.parse(body);
        var btc_price = (data.last);
        var myquery = { slug: "setting" };
        var newvalues = { btc_price: btc_price };
        Setting.updateOne(myquery, newvalues, function(err, res) {
             //console.log("btc updted");
        });
    });
    request('https://www.bitstamp.net/api/v2/ticker/etheur', (error, response, body) => {
        // parse the json answer and get the current bitcoin value
        const data = JSON.parse(body);
        var eth_price = (data.last);
        var myquery = { slug: "setting" };
        var newvalues = { eth_price: eth_price };
        Setting.updateOne(myquery, newvalues, function(err, res) {
            //console.log("eth updted");
        });
    });
    request('https://www.bitstamp.net/api/v2/ticker/eurusd', (error, response, body) => {
        // parse the json answer and get the current bitcoin value
        const data = JSON.parse(body);
        var usd_price = (1 / data.last);
        var myquery = { slug: "setting" };
        var newvalues = { usd_price: usd_price };
        Setting.updateOne(myquery, newvalues, function(err, res) {
            // console.log("usd updted");
        });
    });
})


//The 404 Route (ALWAYS Keep this as the last route)
app.get('*', function(req, res){

     res.locals = {
        title: 'LOOiX | 404'
    };
     res.render('home/404',{ });
});




app.listen(3000, function() {
    console.log('Server Started | 3000!!');
});